#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import socket
import json
import pkg_resources
from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from simulation.replay_simulator import ReplaySimulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.analyzing_simulator import SwitchDatabase
from simulation.timer import Timer
from bots.constant_throttle import ConstantThrottle as ConstantThrottleBot 
from bots.lane_switcher import LaneSwitcher as LaneSwitcherBot
from bots.jyckegarden import Jyckegarden
from bots.special_case_bots import SwitchAndCrashBeforeNext, SwitchDuringCrash, ActivateTurboThatWasAvailableBeforeCrash

class ServerRace(object):


    def __init__(self, host, port, botname, botkey, trackname = None, log_file = None, password = "jyckegarden", car_count = 1):
        print host, port, botname, botkey

        self.json_log = []
        
        self.botname = botname
        self.botkey = botkey
        if trackname:
            self.trackname = trackname
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.socket.connect((host, int(port)))
        self.socket_file = self.socket.makefile()
        self.simulator = None
        if log_file is not None:
            self.log_file = open(log_file, "w")
        else:
            self.log_file = None
        
        self.password = password
        self.car_count = int(car_count)

        self.race_started = False
        self.tournament_ended = False
        self.game_tick = 0
        self.switches = SwitchDatabase.load(pkg_resources.resource_filename("switches", "switches.bin"))
        
        print trackname
        if trackname is not None:
            if car_count==1:
                self.create()
            else:
                self.join_multiplayer()
        else:
            self.join()
        while not self.race_started:
            self.process_next_message()

        
    def tick(self):
        current_tick = self.simulator.get_current_tick_nr()
        while current_tick==self.simulator.get_current_tick_nr() and not self.tournament_ended:
            self.process_next_message()
        
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def msg_with_tick(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.game_tick}))

        
    def send(self, msg):
        self.json_log.append(msg)
        msg = msg +"\n"
        if self.log_file:
            self.log_file.write(msg)
        self.socket.sendall(msg)        
     
    def create(self):
        print self.car_count
        print self.trackname
        self.msg("createRace", {"botId" : {"name": self.botname,"key": self.botkey}, "trackName": self.trackname, "password": self.password, "carCount": self.car_count})

    def join_multiplayer(self):
        self.msg("joinRace", {"botId" : {"name": self.botname,"key": self.botkey}, "trackName": self.trackname, "password": self.password, "carCount": self.car_count})

        
    def join(self):
        self.msg("join", {"name": self.botname,"key": self.botkey})
        
    def on_join(self, data):
        print("Joined")
        pass

    def on_game_start(self, data):
        print("Race started")
        self.race_started = True
        json = "\n".join(self.json_log)
        track = TrackBuilder().build(json)
        cars = CarBuilder().build(json)
        def line_iter():
            pos = 0
            while pos < len(self.json_log):
                lines = self.json_log[pos:]
                pos = len(self.json_log)
                for line in lines:
                    yield line
                    
        car_colors = [car.color for car in cars]
        self.car_index = car_colors.index(self.color)
        
        #self.bot = ConstantThrottleBot(0.6)
        #self.bot = LaneSwitcherBot(0.63)
        print self.car_index, self.color
        
        self.bot = Jyckegarden()
        
        bots = [self.bot if i==self.car_index else None for i in xrange(len(cars))]
        
        old_simulator = self.simulator
        
        
        source_simulator = ReplaySimulator(track = track, cars = cars, owner = self.car_index, line_iter = line_iter())
        self.simulator = AnalyzingSimulator(source_simulator, car_to_analyze = self.car_index, bots = bots, switch_database = self.switches)
        self.bot.analyzer = self.simulator
        self.track = self.simulator.track
        self.cars = self.simulator.cars
        if old_simulator is not None:
            self.simulator.slipconstants_found = old_simulator.slipconstants_found
            self.simulator.slipconstants = old_simulator.slipconstants
            self.simulator.engine_power = old_simulator.engine_power
            self.simulator.drag = old_simulator.drag
            self.simulator.highestc4 = old_simulator.highestc4
            self.simulator.lowestc4 = old_simulator.lowestc4
        
        self.send_server_command()
        
 
    def on_car_positions(self, data):
        if self.simulator is not None:
            self.simulator.tick()
            self.send_server_command()
    
    def on_your_car(self, data):
        self.color = data["color"]
    
    def send_server_command(self):
        if self.bot.command is not None:
            command, value = self.bot.command
            if command == "throttle":
                self.msg_with_tick("throttle", value)
            elif command == "switch":
                if value==-1:
                    self.msg_with_tick("switchLane", "Left")
                else:
                    self.msg_with_tick("switchLane", "Right")
            elif command == "turbo":
                self.msg_with_tick("turbo", "Activating turbo")

    def on_crash(self, data):
        print("Someone crashed")
        pass

    def on_game_end(self, data):
        self.race_started = False
        print("Race ended")
        self.json_log = [] # clean the log, so that new tracks and cars can be created
        while not self.race_started and not self.tournament_ended:
            self.process_next_message()
    
    def on_tournament_end(self, data):
        print("Tournament ended")
        self.tournament_ended = True
        pass

    def on_error(self, data):
        print("Error: {0}".format(data))
        pass        
        
    def process_next_message(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'yourCar': self.on_your_car,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'error': self.on_error,
        }
        line = self.socket_file.readline()
        if line:
            if self.log_file:
                self.log_file.write(line)
            self.json_log.append(line.replace("\n", ""))
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.game_tick = msg["gameTick"]
            
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))



