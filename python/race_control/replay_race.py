#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import json
import copy

from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from simulation.replay_simulator import ReplaySimulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.timer import Timer
from bots.jyckegarden import Jyckegarden

class ReplayRace(object):

    def __init__(self, replay_file, bot = None):
        
        self.simulator = None
        self.old_simulator = None
        self.bot_simulator = None
        self.bot_color = bot
        self.state = ""
        with open(replay_file, "r") as f:
            self.json_log = []
            lines = f.read().splitlines()
            self.line_iter = (line for line in lines)
            self.game_tick = -1
            self.got_positions = False
            self.tick()
        
    def tick(self):
        start_tick = self.game_tick
        while self.game_tick==start_tick:
            self.process_next_message()

    def on_game_start(self, data):
        self.state="started"
        print "Game started"
        json = "\n".join(self.json_log)
        track = TrackBuilder().build(json)
        cars = CarBuilder().build(json)
        
        def line_iter():
            pos = 0
            while pos < len(self.json_log):
                lines = self.json_log[pos:]
                pos = len(self.json_log)
                for line in lines:
                    yield line
        
        if self.bot_color is None:
            self.bot_color = cars[0].color
    
        
        if self.bot_color is not None:
            
            colors = [car.color for car in cars]
            car_index = colors.index(self.bot_color)
            self.car_index = car_index

            source_simulator = ReplaySimulator(track = track, cars = cars, owner=car_index, line_iter = line_iter())
            self.simulator = AnalyzingSimulator(source_simulator)
            self.track = self.simulator.track
            self.cars = self.simulator.cars

    def on_game_end(self, data):
        self.game_tick = -1
        self.json_log = []
        self.simulator = None
        self.old_simulator = self.bot_simulator
        self.bot_simulator = None
        self.state="ended"
        pass
    
    def on_car_positions(self, data):
        print self.game_tick
        if self.simulator is not None:
            self.simulator.tick()

    def on_tournament_end(self, data):
        self.state="tournament_ended"
        pass
    
    def on_error(self, data):
        pass
    
    def process_next_message(self):
        msg_map = {
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'fullCarPositions': self.on_car_positions,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'error': self.on_error,
        }
        line = self.line_iter.next()
        if line:
            self.json_log.append(line)
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.game_tick = msg["gameTick"]
            
            if msg_type in msg_map:
                msg_map[msg_type](data)
