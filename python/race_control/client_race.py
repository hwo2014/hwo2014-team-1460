#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import pkg_resources
import copy
from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from simulation.simulator import Simulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.switch_database import SwitchDatabase
from bots.constant_throttle import ConstantThrottle as ConstantThrottleBot 
from bots.jyckegarden import Jyckegarden
from bots.lane_switcher import LaneSwitcher
from bots.special_case_bots import SwitchAndCrashBeforeNext, SwitchDuringCrash
from race_control.replay_race import ReplayRace
import settings.default


class ClientRace(object):

    def __init__(self, track_file):
        replay = False

        switches = SwitchDatabase.load(pkg_resources.resource_filename("switches", "switches.bin"))
        self.replayrace = None
        if replay:
            replayrace=ReplayRace("testraces/round2_race1.txt", bot="green")
            while replayrace.state!="ended":
                replayrace.tick()
            while(replayrace.state!="started"):
                replayrace.tick()
            
            self.replayrace = replayrace

            source_simulator = Simulator(track = replayrace.simulator.track, cars = copy.deepcopy(replayrace.simulator.cars), crash_duration = 400, switch_database = switches, external_simulator=(replayrace.car_index, replayrace.simulator))
            source_simulator.engine_power = 5.0
            source_simulator.drag = 1.02
            source_simulator.slip_constants = (-0.00125, -0.1, 0.573937211665, 0.463487341845, -0.3)
            bots = [Jyckegarden() if i==replayrace.car_index else None for i in xrange(len(source_simulator.cars))]
            self.simulator = AnalyzingSimulator(source_simulator, bots = bots, switch_database = copy.deepcopy(switches), car_to_analyze = replayrace.car_index)
            self.track = self.simulator.track
            self.cars = self.simulator.cars

        else:
            with open(track_file, "r") as f:
                jsonstring = f.read()
            
                track = TrackBuilder().build(jsonstring)
                cars = CarBuilder().build(jsonstring)
                source_simulator = Simulator(track = track, cars = cars, crash_duration = 20, switch_database = switches, turbo_delay = 500)
                #source_simulator.engine_power = 4.38596491228
                #source_simulator.drag = 1.02
                source_simulator.engine_power = settings.default.engine_power
                source_simulator.drag = settings.default.drag
                source_simulator.slip_constants = settings.default.slipconstants
                #source_simulator.slip_constants = (-0.0012499999999999734, -0.10000000000000188, 0.55999487722592001, 0.5, -0.29999999999999138)
                #bot = ConstantThrottleBot(0.65)
                
                self.simulator = AnalyzingSimulator(source_simulator, bots = [LaneSwitcher()], switch_database = copy.deepcopy(switches))
                self.track = self.simulator.track
                self.cars = self.simulator.cars
            
    
    def tick(self):
        if self.replayrace:
            self.replayrace.tick()
        self.simulator.tick()
        if self.simulator.cars[0].angle >=60.0:
            raise Exception