#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
from bots.base_bot import BaseBot
import numpy as np

class SwitchAndCrashBeforeNext(BaseBot):

    def __init__(self):
        super(SwitchAndCrashBeforeNext, self).__init__()
        
    def update(self, car, simulator):
        track = simulator.track
        if track.pieces[car.piece].is_switch:
            return self.switch(1)
        else:
            return self.throttle(1.0)
        
class SwitchDuringCrash(BaseBot):

    def __init__(self):
        super(SwitchDuringCrash, self).__init__()
        
    def update(self, car, simulator):
        if car.crash_duration > 0 and car.switch==0:
            return self.switch(1)
        else:
            return self.throttle(1.0)
        
class ActivateTurboThatWasAvailableBeforeCrash(BaseBot):

    def __init__(self):
        super(ActivateTurboThatWasAvailableBeforeCrash, self).__init__()
        self.crashed = False
        
    def update(self, car, simulator):
        if car.crash_duration!=0:
            self.crashed = True
        
        if car.turbo_available:
            return self.throttle(1.0)

        if self.crashed and car.throttle==1.0:
            self.crashed = False
            return self.turbo()
        
        engine_power, drag = simulator.get_engine_power_and_drag()
        if np.isclose(engine_power, 0) and np.isclose(drag, 0):
            return self.throttle(1.0)
        elif simulator.slipconstants_found:
            return self.drive_on_limits(simulator, car, [])
        else:
            return self.find_limits(simulator, car)