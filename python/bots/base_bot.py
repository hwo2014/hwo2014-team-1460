#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import copy
from simulation.simulator import Simulator
import settings.default
import numpy as np

class BaseBot(object):

    def __init__(self):
        self.initial_target_c4 = 0.10
        self.target_c4 = self.initial_target_c4
        self.command = None
        self.predicted_car = None
        
    def throttle(self, t):
        self.command = ("throttle", t)
        return self.command
    
    def switch(self, v):
        self.command = ("switch", v)
        return self.command
    
    def turbo(self):
        self.command = ("turbo", True)
        return self.command
        
    def calculate_c4(self, sim, car):
        current_piece = sim.track.pieces[car.piece]
        if current_piece.is_switch and current_piece.is_curve() and  car.lane!=car.end_lane:
            lanes = sim.track.track_settings.lanes
            curve_radius = sim.switch_database.get_curve_radius(current_piece, car.piece_distance, lanes[car.lane], lanes[car.end_lane])
        else:
            curve_radius = current_piece.get_signed_curve_radius(sim.track.track_settings.lanes[car.lane])
        
        if curve_radius != 0.0:
            return car.speed**2 / np.abs(curve_radius)
        else:
            return 0.0
    
    def copy_simulator(self, simulator, car):
        engine_power, drag = simulator.get_engine_power_and_drag()
        sim = Simulator(track=simulator.track, bots = [], cars=[copy.deepcopy(car)], switch_database=simulator.switch_database)
        sim.cars = [copy.deepcopy(car)] # kludge need to set twice, as the simulator constructor messes it up
        sim_car = sim.cars[0] # since we always have only one car
        sim.engine_power = engine_power
        sim.drag = drag
        sim.slip_constants = simulator.get_slipping_coefficients()
        return (sim, sim_car)
    
    def simulate(self, simulator, car, num_steps, num_throttle, limit, switches=[]):
        #TODO, if very close to some limit, like the c4 then change the the throttle
        #another limit, is the piece border, if they have different radiuses
        #print "Start predict", len(switches), car.switch
        self.prediction = None
        for i in xrange(num_steps):
            if car.speed < 0.01:
                break
            if len(switches)>0 and car.switch == 0:
                car.switch = switches[0] - car.end_lane 
                switches = switches[1:]
                #print "Predicting switch", car.switch
            
            if num_throttle > 0:
                car.throttle = 1.0
                num_throttle -= 1
            else:
                car.throttle = 0.0
            simulator.tick()
            if not self.prediction:
                self.prediction = copy.deepcopy(car)
            #print "Simulate", i, car.angle
            
            if limit(car):
                return False
        return True
    
    def find_limits(self, simulator, car):
        sim, sim_car = self.copy_simulator(simulator, car)
        limit_finding_speed = 0.02
        
        sim.slip_constants = settings.default.slipconstants
        if car.crash_duration > 0:
            simulator.reset_slip_constants()
            self.target_c4 = self.initial_target_c4
        
        def limit(c):
            c4 = self.calculate_c4(sim, c)
            return c4 > self.target_c4+limit_finding_speed
        
        if self.simulate(sim, sim_car, 100, 1, limit) and np.abs(car.angle)<20.0:
            c4 = self.calculate_c4(sim, car)
            if c4 > self.target_c4:
                self.target_c4+=limit_finding_speed
            return self.throttle(1.0)
        else:
            return self.throttle(0.0)
        
    def drive_on_limits(self, simulator, car, switches):
        safety_margin = 0.0
        print len(switches)
        
        if self.predicted_car is not None:
            slipaccdiff = abs(self.predicted_car.slipacc-car.slipacc)
            slipaccdiff = min(slipaccdiff, 0.5)
            normalized = slipaccdiff / 0.5
            safety_margin = 50.0*normalized
            #print slipaccdiff
            self.predicted_car = None
        
        #print "%.3f" % safety_margin
        
        sim, sim_car = self.copy_simulator(simulator, car)
        def limit(c):
            return np.abs(c.angle)>(59.9-safety_margin)
        
        can_use_turbo = car.turbo_available is not None and car.turbo_duration == 0 and car.throttle==1.0 and car.turbo==0
        if can_use_turbo:
            sim_car.turbo_factor = car.turbo_available[1]
            sim_car.turbo_duration = car.turbo_available[0]
            if self.simulate(sim, sim_car, 200, int(sim_car.turbo_duration * 0.3), limit, copy.deepcopy(switches)):
                self.predicted_car = self.prediction
                print "Turbo"
                return self.turbo()
            else:
                sim, sim_car = self.copy_simulator(simulator, car)
        
        if self.simulate(sim, sim_car, 200, 1, limit, copy.deepcopy(switches)):
            #print "accelerating"
            self.predicted_car = self.prediction
            return self.throttle(1.0)
        else:
            if car.speed<1.0 and np.abs(car.angle)<1.0 and np.abs(car.slipspeed) < 1.0:
                if simulator.slipconstants_found:
                    simulator.reset_slip_constants()
                    self.target_c4 = self.initial_target_c4
                    print "resetting simulator parameters"
            #print "braking"
            sim, sim_car = self.copy_simulator(simulator, car)
            sim_car.throttle=0
            sim.tick()
            self.predicted_car = copy.deepcopy(sim_car)                
            return self.throttle(0.0)
        
