#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

from bots.base_bot import BaseBot
import numpy as np
from simulation.path_finder import PathFinder

class Jyckegarden(BaseBot):

    def __init__(self):
        super(Jyckegarden, self).__init__()
        self.switch_direction = 1
    
    def update(self, car, simulator):
        #For safety don't run anything else than throttle during crashes
        if car.crash_duration!=0:
            return self.throttle(1.0)
        
        
        engine_power, drag = simulator.get_engine_power_and_drag()
        if np.isclose(engine_power, 0) and np.isclose(drag, 0):
            return self.throttle(1.0)
        elif simulator.slipconstants_found:
            path_finder = PathFinder(simulator.track, simulator.switch_database)
            best_lane=path_finder.find_path(car.piece, car.end_lane)
            wanted_switch = best_lane - car.end_lane
            
            #TODO simulate with best lane
            
            #if very close 7 decimals to either the slip limit or piece change(and piece radius is different) 
            #piece radius is assumed different if prev==switch
            #then run again with 0.99 throttle
            command = self.drive_on_limits(simulator, car, [best_lane])
            
            if car.switch==0 and wanted_switch!=0:
                sim, sim_car = self.copy_simulator(simulator, car)
                sim_car.throttle = 1.0
                num_ticks_to_switch = 0
                track = simulator.track
                start_piece = car.piece
                for i in xrange(3):
                    sim.tick()
                    if sim_car.piece!=start_piece:
                        piece = track.pieces[sim_car.piece]
                        if piece.is_switch:
                            break
                    num_ticks_to_switch+=1
                
                if num_ticks_to_switch == 2:
                    c, v = command
                    if (c=="throttle" and v!=0.0) or c=="turbo":
                        print "Switching", wanted_switch, num_ticks_to_switch
                        print command, car.throttle
                        return self.switch(wanted_switch)
                elif num_ticks_to_switch == 1:
                    print "Switching", wanted_switch, num_ticks_to_switch
                    print command, car.throttle
                    return self.switch(wanted_switch)
            
            #print command
            return command
        else:
            return self.find_limits(simulator, car)
