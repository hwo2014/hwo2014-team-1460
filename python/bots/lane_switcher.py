#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
from bots.base_bot import BaseBot

import numpy as np
import copy

class LaneSwitcher(BaseBot):

    def __init__(self):
        super(LaneSwitcher, self).__init__()
        self.old_piece = None
        self.switch_direction = 1
        self.switches = None
    
    def get_next_switch(self, simulator, car):
        track = simulator.track
        database = simulator.switch_database
        
        class Switch():
            def __init__(self, piece, piece_index):
                self.piece = piece
                self.piece_index = piece_index
                num_lanes = len(track.track_settings.lanes)
                self.use_count = [[0 for _ in xrange(num_lanes)] for _ in xrange(num_lanes)]
                
        
        if self.switches is None:
            self.switches = [ Switch(piece, index) for index, piece in enumerate(track.pieces) if piece.is_switch]

        if len(self.switches)==0:
            return 0

        def distance_key(switch):
            key1 = switch.piece_index <= car.piece
            key2 = switch.piece_index
            return key1*10000 + key2

        lanes = track.track_settings.lanes
        switches = [(switch, switch_index, lane1, lane2, abs(lane1-car.lane)<=switch_index, database.is_fully_known(switch.piece, lanes[lane1], lanes[lane2]))
                       for switch_index, switch in enumerate(sorted(self.switches, key = distance_key))
                       for lane1 in xrange(len(track.track_settings.lanes)) 
                       for lane2 in xrange(len(track.track_settings.lanes))
                       if abs(lane1-lane2)==1
                   ]

   
        def sort_key(s):
            switch, switch_index, lane1, lane2, usable, known = s
            key1 = known
            key2 = switch.use_count[lane1][lane2]
            key3 = not usable
            return key1*1000000 + key2*100000 + key3*10000
            
        switches = sorted(switches, key=sort_key)
        next_switch = copy.deepcopy(switches[0])
        n_switch, n_switch_index, n_lane, n_lane2, n_usable, n_known = next_switch        
        
        def sort_key2(s):
            switch, switch_index, lane1, lane2, usable, known = s
            return n_switch_index > switch_index and np.abs(lane1 - n_lane2)>= (n_switch_index - switch_index)
        
        
        switches = sorted(switches, key=sort_key2)
        
        switch, switch_index, lane1, lane2, usable, known = switches[0]
        
        print switch.piece_index, switch.use_count[lane1][lane2], switch_index, lane1, lane2, usable, known, car.lane
        
        if switch_index==0:
            if lane2>lane1:
                return 1
            if lane2<lane1:
                return -1
        else:
            if lane1>car.lane:
                return 1
            elif lane1<car.lane:
                return -1
        return 0
        
    def update(self, car, simulator):
        
        track = simulator.track

        engine_power, drag = simulator.get_engine_power_and_drag()
        
        if np.isclose(engine_power, 0) and np.isclose(drag, 0):
            return self.throttle(1.0)
        elif simulator.slipconstants_found:
            best_lane = self.get_next_switch(simulator, car)
            wanted_switch = best_lane - car.end_lane
            command = self.drive_on_limits(simulator, car, [wanted_switch])
            if command[0]=="throttle" and command[1] == car.throttle:
                current_piece = track.pieces[car.piece]
                next_piece = track.get_next_piece(current_piece)
                if current_piece!=self.old_piece:
                    self.old_piece = current_piece
                    if current_piece.is_switch and self.switches is not None:
                        for switch in self.switches:
                            if switch.piece == current_piece:
                                switch.use_count[car.lane][car.end_lane]+=1
                                break
                    if next_piece.is_switch:
                        print "next is switch"
                        switch = self.get_next_switch(simulator, car)
                        if switch!=0:
                            return self.switch(switch)
                    
                self.old_piece = current_piece
            return command
        else:
            return self.find_limits(simulator, car)
        

        