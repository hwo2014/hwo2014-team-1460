#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import json
from bots.base_bot import BaseBot

class ReplayBot(BaseBot):

    def __init__(self, jsontext):
        super(ReplayBot, self).__init__()
        self.line_iter = iter(jsontext.splitlines())
        self.hickup = 0

    def update(self, car, simulator):
        if self.hickup<0:
            self.hickup +=1
        elif self.hickup>0:
            hickup = self.hickup
            self.hickup=0
            for _ in xrange(hickup):
                self.update(car, simulator)
            return self.update(car, simulator)
        else:
            for line in self.line_iter:
                msg = json.loads(line)
                msg_type, data = msg['msgType'], msg['data']
                if msg_type == "throttle":
                    return self.throttle(data)
                if msg_type == "switchLane":
                    if data == "Left":
                        return self.switch(-1)
                    elif data == "Right":
                        return self.switch(1)
                if msg_type == "turbo":
                    print "Activating turbo"
                    return self.turbo()

                  
