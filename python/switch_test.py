'''
Created on 27 apr 2014

@author: Fred Wales
'''

import json
import collections
import pkg_resources
import math


from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.replay_simulator import ReplaySimulator
import simulation.math2d as math2d

import pandas
import matplotlib.pyplot as plt
import numpy as np
import scipy

if __name__ == '__main__':
    #with open(pkg_resources.resource_filename("simulation", "race_simulation_tests/lane_switching_full_speed.txt" ), "r") as f:
    with open("testraces/pentagswitch.txt", "r") as f:
        jsonstring = f.read()
        track = TrackBuilder().build(jsonstring)
        cars = CarBuilder().build(jsonstring)        
        analyzer = AnalyzingSimulator(ReplaySimulator(track = track, cars = cars, owner=0, jsontext = jsonstring))
        analyzer.tick_until_done()
        database = analyzer.switch_database

        
        #curve = database.curve_angle_database[(100, 45.0, 10, -10)]
        
        for key, value in database.curve_database.items():
            print key, value

        normalized = True
        xy=False
        
       
        if normalized:
            for key, value in database.curve_angle_database.items():
                if np.abs(key[1])!=45:
                    continue
                #if key[1]<0:
                #    continue

                #fig = plt.figure()
                #ax = fig.add_subplot(111, aspect="equal", adjustable="datalim")


                print key
                #print key[0]+key[2], key[0]+key[3]       
                x, y = zip(*value)
                dist = database.curve_database[key]
                print dist
                x = np.array(x)
                x = x / dist
                x=np.floor(x*100.0)/100.0
                
                def func(x, a, b, c):
                    return a*np.exp(-b*x) + c
                
                def f(x, a, b, c, d, e):
                    #return d*(x+e)+a+b*(x+c)**2
                    #return a+b*x+c*x**2+d*x**3
                    return (1.0-x)**3*a+3*(1.0-x)**2*x*b+3*(1.0-x)*x**2*c+x**3*d 
                try:
                    popt, pcov = scipy.optimize.curve_fit(f, x, y)
                    a, b, c, d, e=popt
                    print a, b, c, d, e
                    
                    print b/c
                    
                    plt.plot(x, y, "-o")
                    plt.plot(x, [key[0]+key[2]]*len(x))
                    plt.plot(x, [key[0]+key[3]]*len(x))
                    t = np.arange(0.0, 1.01, 0.01)
                    radius = [f(x1, a, b, c, d, e) for x1 in t]
                    plt.plot(t, radius)
                    
                    print radius[0], radius[-1]
                    if False:
                        def xpos(t):
                            angle = math.radians(key[1])*t
                            radius = f(t, a, b, c, d, e)
                            #x=math.sin(angle)*radius
                            n = np.array((0, radius))
                            v=math2d.rotate_vector_rad(n, angle)
                            return v[0]
    
                        def ypos(t):
                            angle = math.radians(key[1])*t
                            radius = f(t, a, b, c, d, e)
                            #y=math.cos(angle)*radius
                            #y-=radius
                            n = np.array((0, radius))
                            v=math2d.rotate_vector_rad(n, angle)
                            return v[1]
    
                        def xpos2(t):
                            angle = math.radians(key[1])*t
                            radius = key[0]+key[2]
                            #x=math.sin(angle)*radius
                            n = np.array((0, radius))
                            v=math2d.rotate_vector_rad(n, angle)
                            return v[0]
    
                        def ypos2(t):
                            angle = math.radians(key[1])*t
                            radius = key[0]+key[2]
                            #y=math.cos(angle)*radius
                            #y-=radius
                            n = np.array((0, radius))
                            v=math2d.rotate_vector_rad(n, angle)
                            return v[1]
                        
                        def xpos3(t):
                            angle = math.radians(key[1])*t
                            radius = key[0]+key[3]
                            #x=math.sin(angle)*radius
                            n = np.array((0, radius))
                            v=math2d.rotate_vector_rad(n, angle)
                            return v[0]
    
                        def ypos3(t):
                            angle = math.radians(key[1])*t
                            radius = key[0]+key[3]
                            #y=math.cos(angle)*radius
                            #y-=radius
                            n = np.array((0, radius))
                            v=math2d.rotate_vector_rad(n, angle)
                            return v[1] 
    
                        xs = [0.0]
                        ys = [key[2]+key[0]]
                        arc_length = dist / 100.0
                        prev_angle = 0
                        tangent = np.array((1.0, 0.0))
                        prev_pos = np.array((0.0, key[2]))
                        
                        total_length = 0.0
                        for t1 in t:
                            radius = f(t1, a, b, c, d, e)
                            #radius = 70.0
                            circumference = 2.0*np.pi*radius
                            #angle = (arc_length / circumference)*2.0*np.pi
                            #angle = key[1] / 100.0
                            #print math.degrees(angle), radius, arc_length, prev_angle
                            
                            n = np.array((0, radius))
                            #v=math2d.rotate_vector_rad(n, prev_angle)
                            #v2=math2d.rotate_vector_rad(n, angle+prev_angle)
                            v=math2d.rotate_vector_deg(n, key[1]*t1)
                            v2=math2d.rotate_vector_deg(n, key[1]*(t1+0.01))
                            
                            #print math.degrees(prev_angle), math.degrees(angle)
                            
                            v = v2-v
                            
                            #x = math.sin(angle) * radius
                            #y = math.cos(angle) * radius
                            #y-=radius
                            #total_length+=np.sqrt(x**2 + y**2)
                            #total_length += arc_length
                            #print total_length, np.sqrt(x**2 + y**2), arc_length, x,  y
                            
    
                            #vect = np.array((x, y))#*tangent
                            #vect=math2d.rotate_vector_rad(vect, prev_angle)
                            
                            #print vect
                            #prev_pos = prev_pos + vect
                            #print prev_pos
                            #tangent=math2d.rotate_vector_rad(tangent, angle)
                            #prev_angle += angle
                            xs.append(xs[-1]+v[0])
                            ys.append(ys[-1]+v[1])
                        
                        #x=[xpos(t1) for t1 in t]
                        #y=[ypos(t1) for t1 in t]
                        plt.plot(xs, ys)
                        x=[xpos2(t1) for t1 in t]
                        y=[ypos2(t1) for t1 in t]
                        plt.plot(x, y)
                        x=[xpos3(t1) for t1 in t]
                        y=[ypos3(t1) for t1 in t]
                        plt.plot(x, y)
                    
                    plt.show()
                except:
                    print "Failed to solve"
                    pass
        if xy:
            for key, value in database.curve_angle_database.items():
                prev_dist = 0.0
                xs = []
                ys = []
                
                xs_1 = []
                ys_2 = []
                
                prev_pos = np.array((0.0, key[2]))
                prev_dist = 0.0
                curve_length = database.curve_database[key]
                tangent = np.array((1.0, 0.0))
                for dist, radius in value:
                    circumference = 2.0*np.pi*radius
                    arc_len = dist-prev_dist
                    angle = arc_len / circumference
                    x = math.sin(angle) * radius
                    y = math.cos(angle) * radius
                    y-=radius
                    
                    vect = np.array((x, y))*tangent
                    #print vect
                    prev_pos = prev_pos + vect
                    tangent=math2d.rotate_vector_rad(tangent, angle)
                    print tangent
                    #print prev_pos
                    prev_dist = dist
                    
                    xs.append(prev_pos[0])
                    ys.append(prev_pos[1])
                    
                    
                    normalized_pos = dist / curve_length
                    circumference = 2.0*np.pi*(key[0]+key[2])
                    angle=key[1]*normalized_pos
                    angle = circumference
                    
                  
                    
                    
                    #print tangent
                    
                    #x = math.sin(angle) * radius
                    #y = math.cos(angle) * radius
                    #y=radius-y
                    #prev_x+=x
                    #prev_y+=y
                    #print dist, arc_len, math.degrees(angle), prev_x, prev_y, np.sqrt(prev_x**2 + prev_y**2)
                    #xs.append(prev_x)
                    #ys.append(prev_y)
                    #prev_dist = dist
                    #prev_x = x
                    #prev_y = y
                
                #print xs
                #print ys
                plt.plot(xs, ys, "-o")
                #break
                
        plt.show()
        
        #for curve in database.curve_angle_database:
        #    print curve