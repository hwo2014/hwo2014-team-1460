#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

from pyglet.gl import *
import numpy as np
import webcolors

class CarVisualizer(object):

    def __init__(self, race):
        self.race = race
        
    def draw(self):
        for car in self.race.cars:
            bounding_box = car.get_bounding_box()
            color = webcolors.name_to_rgb(car.color)
            color = [x / 255.0 for x in color] 
            
            glColor3f(*color)
            glBegin(GL_QUADS)
            for x, y in bounding_box:
                glVertex2f(x, y)
            glEnd()
