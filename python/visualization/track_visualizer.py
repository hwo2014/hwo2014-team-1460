#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

from pyglet.gl import *
import numpy as np

class TrackVisualizer(object):

    def __init__(
                 self, track, 
                 tarmac_color, 
                 section_joint_color, 
                 section_joint_width,
                 lane_color,
                 lane_width
                 ):
        self.track = track
        self.tarmac_color = tarmac_color
        self.section_joint_color = section_joint_color
        self.section_joint_width = section_joint_width
        self.lane_color = lane_color
        self.lane_width = lane_width
        
        self.generate_tarmac_buffer()
        self.generate_lane_buffer()
        self.generate_switch_lane_buffers()
        self.generate_section_joint_buffer()
    
    def draw(self):
        self.draw_tarmac()
        self.draw_lanes()
        self.draw_section_joints()
        
    def step_pieces(self, func):
        for piece in self.track.pieces:
            
            if piece.is_straight():
                step_size = 1.0
            else:
                step_size = 1.0 / np.abs(piece.curve_angle * 2.0)
            
            pos = 0.0
            while pos<=1.0:
                if pos + step_size > 1.0:
                    pos = 1.0
                
                func(piece, pos)
                
                pos+=step_size        

    def get_extents(self):
        class Namespace(object): pass
        ns = Namespace()
        
        ns.min_ext = (np.finfo(np.float32).max, np.finfo(np.float32).max)
        ns.max_ext = (np.finfo(np.float32).min, np.finfo(np.float32).min)
        
        def func(piece, pos):
            left = piece.get_left_edge(self.track.track_settings.width, pos)
            right = piece.get_right_edge(self.track.track_settings.width, pos)
            ns.min_ext = (np.min((ns.min_ext[0], left[0], right[0])), np.min((ns.min_ext[1], left[1], right[1])))
            ns.max_ext = (np.max((ns.max_ext[0], left[0], right[0])), np.max((ns.max_ext[1], left[1], right[1])))
        
        self.step_pieces(func)

        return (ns.min_ext, ns.max_ext)

    def draw_tarmac(self):
        glColor3f(*self.tarmac_color)
        glEnableClientState(GL_VERTEX_ARRAY)
        glVertexPointer(2, GL_FLOAT, 0, self.tarmac_vertices)
        glDrawArrays(GL_QUAD_STRIP, 0, len(self.tarmac_vertices) / 2)
    
    def generate_tarmac_buffer(self):
        class Namespace(object): pass
        ns = Namespace()
        ns.output = []
        
        def func(piece, pos):
            ns.output.extend(list(piece.get_left_edge(self.track.track_settings.width, pos)))
            ns.output.extend(list(piece.get_right_edge(self.track.track_settings.width, pos)))

        self.step_pieces(func)
        self.tarmac_vertices = (GLfloat * len(ns.output))(*ns.output)

    def generate_section_joint_buffer(self):
        output = []
        for piece in self.track.pieces:
            output.extend(list(piece.get_left_edge(self.track.track_settings.width, 0.0)))
            output.extend(list(piece.get_right_edge(self.track.track_settings.width, 0.0)))
        self.section_joint_vertices = (GLfloat * len(output))(*output)

    def draw_section_joints(self):
        glColor3f(*self.section_joint_color)
        glLineWidth(self.section_joint_width)
        glEnableClientState(GL_VERTEX_ARRAY)
        glVertexPointer(2, GL_FLOAT, 0, self.section_joint_vertices)
        glDrawArrays(GL_LINES, 0, len(self.section_joint_vertices) / 2)
        
    def draw_bezier2(self, points, t, add_point):
        if (len(points) == 1):
            add_point(points[0][0], points[0][1])
        else:
            newpoints = []
            for i in range(0, len(points)-1):
                x = (1-t) * points[i][0] + t * points[i+1][0]
                y = (1-t) * points[i][1] + t * points[i+1][1]
                newpoints.append((x, y))
            self.draw_bezier2(newpoints, t, add_point)
            
    def draw_bezier(self, points, add_point):
        t = 0
        while (t <= 1):
            self.draw_bezier2(points, t, add_point)
            t += 0.01
        
    def generate_lane_buffer(self):
        self.lane_vertices = []
        for lane in self.track.track_settings.lanes:
            class Namespace(object): pass
            ns = Namespace()
            ns.output = []
            def func(piece, pos):
                ns.output.extend(list(piece.get_lane(lane, pos, -self.lane_width / 2.0)))
                ns.output.extend(list(piece.get_lane(lane, pos, self.lane_width / 2.0)))
            
            self.step_pieces(func)
            self.lane_vertices.append((GLfloat * len(ns.output))(*ns.output))

    def generate_switch_lane_buffers(self):
        self.switch_lane_vertices = []
        
        for piece in self.track.pieces:
            if piece.is_switch:
                class Namespace(object): pass
                ns = Namespace()
                ns.output = []
                                
                def add_point(x, y):
                    ns.output.extend(list((x, y)))

                for index, lane in enumerate(self.track.track_settings.lanes[:-1]):                
                    next_lane = self.track.track_settings.lanes[index+1]
                        
                    def generate_switch_lane(lane1, lane2):
                        start = piece.get_lane(lane1, 0.0)
                        end = piece.get_lane(lane2, 1.0)
                        mid1 =  piece.get_lane(lane1, 0.5)
                        mid2 = piece.get_lane(lane2, 0.5)
                        self.draw_bezier([start, mid1, mid2, end], add_point)
                        output = ns.output[:]
                        ns.output = []
                        self.switch_lane_vertices.append((GLfloat * len(output))(*output))
                    generate_switch_lane(lane, next_lane)
                    generate_switch_lane(next_lane, lane)
        
        
    def draw_lanes(self):
        glColor3f(*self.lane_color)
        for lane in xrange(len(self.track.track_settings.lanes)):
            glEnableClientState(GL_VERTEX_ARRAY)
            glVertexPointer(2, GL_FLOAT, 0, self.lane_vertices[lane])
            glDrawArrays(GL_QUAD_STRIP, 0, len(self.lane_vertices[lane]) / 2)
        for switch_lane in self.switch_lane_vertices:
            glEnableClientState(GL_VERTEX_ARRAY)
            glVertexPointer(2, GL_FLOAT, 0, switch_lane)
            glDrawArrays(GL_LINE_STRIP, 0, len(switch_lane) / 2)

