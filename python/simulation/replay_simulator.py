#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

from simulation.simulator_base import SimulatorBase
import json

class ReplaySimulator(SimulatorBase):

    def __init__(self, track, cars, owner, jsontext = None, line_iter = None, bots=[]):
        super(ReplaySimulator, self).__init__(track, cars, bots)
        if jsontext is None:
            self.line_iter = line_iter
        else:
            self.line_iter = iter(jsontext.splitlines())
        self.last_throttle = 0
        self.turbo_available = None
        self.turbo_start = []
        self.turbo_end = []
        self.crash = []
        self.spawn = []
        self.switch = 0
        self.owner = owner
        for car in self.cars:
            car.turbo = False
            car.turbo_duration = 0
            car.turbo_factor = 1.0
            car.crash_duration = 0
            car.turbo_available = None
            car.turbo_activation = None
            car.end_lane = None
            car.switch = 0
            car.throttle = 0.0
        
        self.tick()

        
    def tick(self):
        data = self.read_next_car_position_message()
        if data is not None:
            for car in self.cars:
                idx = -1
                for i, d in enumerate(data):
                    if d["id"]["color"]==car.color:
                        idx =  i
                        break
                if idx!=-1:
                    piece_position=data[idx]["piecePosition"]
                    car.piece = piece_position["pieceIndex"]
                    car.piece_distance = piece_position["inPieceDistance"]
                    lane = piece_position["lane"]
                    old_end_lane = None
                    if car.end_lane is not None:
                        old_end_lane = car.end_lane
                    car.lane = lane["startLaneIndex"]
                    car.end_lane = lane["endLaneIndex"]
                    if old_end_lane is None:
                        old_end_lane = car.end_lane
                    if "angleOffset" in data[idx]:
                        car.angle = data[idx]["angleOffset"]
                    else:
                        car.angle = data[idx]["angle"]
                        
                    if car.turbo_duration > 0:
                        car.turbo_duration-=1
                    else:
                        car.turbo_factor = 1.0

                    if car.turbo:
                        #Just to prevent crashing, in case this happens
                        if car.turbo_available is not None:           
                            car.turbo_factor = car.turbo_available[1]
                            car.turbo_duration = car.turbo_available[0] 
                        car.turbo_available = None 
                        car.turbo = False
    
                    
                    if car.crash_duration > 0:
                        print "Crashed", car.color, car.crash_duration
                        car.crash_duration-=1
                        car.throttle = 0.0
                        car.turbo_available = None
                        car.turbo_duration = 0
                        car.turbo_factor = 0
                        
                    if idx == self.owner:
                        if car.crash_duration ==0:
                            car.throttle = self.last_throttle
                            if self.switch != 0:
                                #print "Actual switch"
                                car.switch = self.switch
                            if car.end_lane != car.lane and car.end_lane != old_end_lane:
                                car.switch = 0                        
                    
                    for crash in self.crash:
                        if crash["color"]==car.color:
                            car.crash_duration = 400
                            car.turbo_available = None
                    for spawn in self.spawn:
                        if spawn["color"]==car.color:
                            car.crash_duration = 0
                    
                    if car.crash_duration==0:
                        if self.turbo_available is not None:
                            print "Setting turbo available " + car.color, car.crash_duration
                            car.turbo_available = (self.turbo_available["turboDurationTicks"], self.turbo_available["turboFactor"])
                        for end in self.turbo_end:
                            if end["color"]==car.color:
                                car.turbo_factor = 1.0
                                car.turbo_duration = 0
                        for start in self.turbo_start:
                            if start["color"]==car.color:
                                car.turbo = True
                
                self.update_car_position_from_piece_distance(car)
                self.update_car_angle_with_slip_angle(car, car.angle)
            self.turbo_start = []
            self.turbo_end = []
            self.crash = []
            self.spawn = []
            self.turbo_available = None
            self.switch = 0
            self.update_bots()
            return True
        else:
            return False
        
            
    def read_next_car_position_message(self):
        for line in self.line_iter:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type == "carPositions" or msg_type == "fullCarPositions":
                return data
            elif msg_type == "throttle":
                self.last_throttle = data
            elif msg_type == "switchLane":
                #print "switchLane", data
                if data=="Left":
                    self.switch = -1
                else:
                    self.switch = 1
            elif msg_type == "turboAvailable":
                self.turbo_available = data
                print "Turbo available"
            elif msg_type == "turboStart":
                self.turbo_start.append(data)
                print "Turbo start " + data["color"]
            elif msg_type == "turboEnd":
                self.turbo_end.append(data)
                print "Turbo end " + data["color"]             
            elif msg_type == "crash":
                self.crash.append(data)
                print "Crash " + data["color"]  
            elif msg_type == "spawn":
                self.spawn.append(data)
                print "spawn " + data["color"]
            elif msg_type == "gameInit":
                print "gameInit"
        print "No data"
        return None
    
