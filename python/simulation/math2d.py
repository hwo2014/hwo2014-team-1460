#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import math
import numpy as np

def rotation_matrix_rad(rot):
    cos_v = math.cos(rot)
    sin_v = math.sin(rot)
    return np.array([[cos_v, sin_v], [-sin_v, cos_v]])

def rotation_matrix_deg(rot):
    return rotation_matrix_rad(math.radians(rot))

def rotate_vector_rad(vect, rot):
    rot_matrix = rotation_matrix_rad(rot)
    return rot_matrix.dot(vect)

def rotate_vector_deg(vect, rot):
    return rotate_vector_rad(vect, math.radians(rot))

def direction_to_angle_rad(direction, zero_angle):
    direction = np.array(direction, dtype=np.float64)
    zero_angle = np.array(zero_angle, dtype=np.float64)
    angle = math.acos(zero_angle.dot(direction))
    cross_product = np.cross(direction, zero_angle)
    if cross_product < 0:
        angle = 2.0*np.pi-angle
    return angle

def direction_to_angle_deg(direction, zero_angle):
    return math.degrees(direction_to_angle_rad(direction, zero_angle))
        
def generate_bezier(points, num_steps):
    def generate_bezier_helper(points, t, output):
        if (len(points) == 1):
            output.append(np.array((points[0][0], points[0][1])))
        else:
            newpoints = []
            for i in range(0, len(points)-1):
                x = (1-t) * points[i][0] + t * points[i+1][0]
                y = (1-t) * points[i][1] + t * points[i+1][1]
                newpoints.append((x, y))
            generate_bezier_helper(newpoints, t, output)

    output = []    
    for t in np.linspace(0.0, 1.0, num=num_steps):
        generate_bezier_helper(points, t, output)
    
    return np.array(output)
