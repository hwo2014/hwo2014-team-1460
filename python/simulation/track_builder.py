#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

from simulation.track import Track
from simulation.track_piece import TrackPiece
import json
import numpy as np

class TrackBuilder(object):

    def __init__(self):
        '''
        Constructor
        '''
    def build(self, jsonstring):
        for line in jsonstring.splitlines():
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            
            if msg_type == "gameInit":
                pieces=data["race"]["track"]["pieces"]
                start_point = data["race"]["track"]["startingPoint"]
                start_pos = (start_point["position"]["x"], start_point["position"]["y"])
                lanes = data["race"]["track"]["lanes"]
                lanes2 = [0]*len(lanes)
                for lane in lanes:
                    lanes2[lane["index"]] = lane["distanceFromCenter"]
    
                min_lane_pos = np.min(lanes2)
                max_lane_pos = np.max(lanes2)
                lane_width = max_lane_pos - min_lane_pos
                track_width = lane_width + 2.0*(lane_width / len(lanes)) 
                
                track = Track(track_width, lanes2)
                track.add_start_point(position=start_pos, angle=start_point["angle"], piece=TrackPiece(**pieces[0]))
                for piece in pieces[1:]:
                    track.add_piece(TrackPiece(**piece))
                return track
        return None    
                
            
        