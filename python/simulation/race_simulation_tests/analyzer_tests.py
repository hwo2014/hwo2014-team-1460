#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
import unittest
import pkg_resources
from simulation.replay_simulator import ReplaySimulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.car_builder import CarBuilder
from simulation.track_builder import TrackBuilder
import numpy as np
import os
import matplotlib.pyplot as plt

class AnalyzerTests(unittest.TestCase):

    
    def prepare_test(self, filename):
        with open(pkg_resources.resource_filename("simulation", os.path.join("race_simulation_tests", filename)), "r") as f:
            jsonstring = f.read()
            track = TrackBuilder().build(jsonstring)
            cars = CarBuilder().build(jsonstring)        
            analyzer = AnalyzingSimulator(ReplaySimulator(track = track, cars = cars, owner=0, jsontext = jsonstring))
            analyzer.tick_until_done()
            self.analyzer = analyzer


    def test_find_start_reaction(self):
        self.prepare_test("constant65.txt")
        self.assertEqual(3, self.analyzer.find_start_reaction_time(0))
        self.prepare_test("randomthrottle.txt")
        self.assertEqual(3, self.analyzer.find_start_reaction_time(0))
            
    def test_find_engine_power(self):
        self.prepare_test("constant65.txt")
        self.assertAlmostEqual(5.0, self.analyzer.get_engine_power_and_drag()[0], 3)
        self.prepare_test("randomthrottle.txt")
        self.assertAlmostEqual(4.766, self.analyzer.get_engine_power_and_drag()[0], 3)
        df = self.analyzer.get_data_for_car(0)
        
        
    def test_find_engine_drag(self):
        self.prepare_test("constant65.txt")
        self.assertAlmostEqual(1.02, self.analyzer.get_engine_power_and_drag()[1], 3)
        self.prepare_test("randomthrottle.txt")
        self.assertAlmostEqual(1.02, self.analyzer.get_engine_power_and_drag()[1], 3)        

        
    def test_find_slipping_coefficients(self):
        self.prepare_test("constant65.txt")
        #-0.00125 -0.1 0.53033008589 0.319709112947 -0.3
        np.testing.assert_almost_equal((-0.001, -0.100, 0.530, 0.320, -0.300), self.analyzer.get_slipping_coefficients(), 3)
        self.prepare_test("randomthrottle.txt")
        #-0.00125 -0.1 0.519336253593 0.333878834062 -0.3
        np.testing.assert_almost_equal((-0.001, -0.100, 0.519, 0.333, -0.300), self.analyzer.get_slipping_coefficients(), 3)    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()