#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
import unittest
import pkg_resources
import copy
import sys
import argparse
import traceback
from simulation.simulator import Simulator
from simulation.replay_simulator import ReplaySimulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.car_builder import CarBuilder
from simulation.track_builder import TrackBuilder
from simulation.switch_database import SwitchDatabase
from bots.replay_bot import ReplayBot

import numpy as np
import matplotlib.pyplot as plt
from IPython.terminal.embed import InteractiveShellEmbed

class FullRaceSimulation(unittest.TestCase):
    enable_ipython = False
    
    def setUp(self):
        self.ignore = False
        self.ignore_always = False
        self.ignore_count = 0
        if self.enable_ipython:
            self.check_equal_state = self.check_equal_state_ipython
        else:
            self.check_equal_state = self.check_equal_state_normal
        
    def check_equal_state_normal(self, replay, simulator):
            r_car = replay.cars[0]
            s_car = simulator.source.cars[0]
            last_analyzed_frame = replay.get_data_for_car(0).tail(1)
            self.assertAlmostEqual(last_analyzed_frame.acceleration.irow(0), simulator.source.cars[0].acceleration, 3)
            self.assertAlmostEqual(last_analyzed_frame.distance.irow(0), simulator.source.cars[0].distance, 2)
            self.assertAlmostEqual(last_analyzed_frame.speed.irow(0), simulator.source.cars[0].speed, 3)
            
            np.testing.assert_almost_equal(r_car.position, s_car.position, 3)
            self.assertEqual(r_car.piece, s_car.piece)
            self.assertAlmostEqual(r_car.piece_distance, s_car.piece_distance, 2)
            np.testing.assert_allclose(r_car.angle, s_car.angle, atol=0.01)

    def check_equal_state_ipython(self, replay, simulator):
        try:
            self.ignore_count-=1
            self.check_equal_state_normal(replay, simulator)
            if self.ignore_count == 0:
                raise Exception
        except Exception as e:
            if self.ignore_always:
                print str(e)
            elif self.ignore_count > 0:
                print str(e)
            else:
                #for easier use from ipython
                s = self.simulator.source
                r = self.replay.source
                sdata = self.simulator.get_data_for_car(0)
                rdata = self.replay.get_data_for_car(0)
                
                ipshell = InteractiveShellEmbed(banner1 = traceback.format_exc(), banner2 = "Set self.ignore or self.ignore_always to True to continue")
                ipshell.enable_matplotlib("auto")
                ipshell()
                if not self.ignore and not self.ignore_always and self.ignore_count==0:
                    raise e
                self.ignore = False

               

    def simulate_with_no_parameters(self, filename):
        with open(pkg_resources.resource_filename("simulation", "race_simulation_tests/" + filename), "r") as f:
            jsonstring = f.read()
            track = TrackBuilder().build(jsonstring)
            cars = CarBuilder().build(jsonstring)        
            analyzer = AnalyzingSimulator(ReplaySimulator(track = track, cars = cars, owner=0, jsontext = jsonstring))
            analyzer.tick_until_done()
            self.analyzer = analyzer
            start_reaction_time = analyzer.find_start_reaction_time(0)
            engine_power, drag = analyzer.get_engine_power_and_drag()
            slip_constants = analyzer.get_slipping_coefficients()
            #analyzer.plot_current(0)
            self.simulate(filename, start_reaction_time, engine_power, drag, slip_constants, [], switch_database = analyzer.switch_database)
            

    def simulate(self, filename, start_reaction_time, engine_power, drag, slipconstants, server_hickups, switch_database=SwitchDatabase()):
        with open(pkg_resources.resource_filename("simulation", "race_simulation_tests/" + filename), "r") as f:
            jsonstring = f.read()
        
            track = TrackBuilder().build(jsonstring)
            cars = CarBuilder().build(jsonstring)        
            replay = AnalyzingSimulator(ReplaySimulator(track = track, cars = copy.deepcopy(cars), owner=0, jsontext = jsonstring), switch_database=copy.deepcopy(switch_database))
            simulator = Simulator(track = track, cars = copy.deepcopy(cars), switch_database=copy.deepcopy(switch_database), crash_duration=400)
            simulator.start_reaction_time = start_reaction_time
            simulator.engine_power = engine_power
            simulator.drag = drag
            simulator.slip_constants= slipconstants
            simulator = AnalyzingSimulator(simulator, bots = [ReplayBot(jsonstring)])
            bot = simulator.bots[0]  
            
            self.replay = replay
            self.simulator = simulator
            tick = 0
            self.check_equal_state(replay, simulator)
            bot.hickup = -start_reaction_time
            old_turbo = replay.cars[0].turbo_available
            
            while replay.tick():
                tick+=1
                print "Tick %i" % tick
               
                if tick in server_hickups:
                    bot.hickup = server_hickups[tick]

                if old_turbo!=replay.cars[0].turbo_available:
                    simulator.source.cars[0].turbo_available = replay.cars[0].turbo_available
                
                self.assertTrue(simulator.tick())
                self.check_equal_state(replay, simulator)       


    def test_constant_65(self):
        self.simulate(
                      filename = "constant65.txt", 
                      start_reaction_time = 3,
                      engine_power = 5,
                      drag = 1.02,
                      slipconstants = (-0.00125, -0.1, 0.53033008589, 0.319709112947, -0.3),
                      server_hickups = {}
                      )
        
    def test_random_throttle(self):
        self.simulate(
                      filename = "randomthrottle.txt", 
                      start_reaction_time = 3,
                      engine_power = 4.76554728,
                      drag = 1.01990393,
                      slipconstants = (-0.00125, -0.1, 0.519336253593, 0.333878834062, -0.3),
                      server_hickups = {109: 2, 602: -1, 609: -1, 744: 1, 1158: -1, 1204: -1, 1572: 1}
                      )
        
    def test_lane_switching(self):
        self.simulate_with_no_parameters(filename = "lane_switching.txt")
        
    def test_lane_switching_full_speed(self):
        self.simulate_with_no_parameters(filename = "lane_switching_full_speed.txt")
        
    def test_strange_switching_turbo_crash(self):
        self.simulate_with_no_parameters(filename = "lane_switching_full_speed.txt")

    def test_switch_and_crash(self):
        self.simulate_with_no_parameters(filename = "switch_and_crash.txt")

    def test_switch_during_crash(self):
        self.simulate_with_no_parameters(filename = "switch_during_crash.txt")

    def test_activate_turbo_available_before_crash(self):
        #this test fails but we will never attempt to do this!
        #self.simulate_with_no_parameters(filename = "activate_turbo_available_before_crash.txt")
        pass
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action="store_true")
    parser.add_argument('unittest_args', nargs='*')

    args = parser.parse_args()
    if args["debug"]==True:
        debugging_enabled = True
   

    # Now set the sys.argv to the unittest_args (leaving sys.argv[0] alone)
    sys.argv[1:] = args.unittest_args
    unittest.main()