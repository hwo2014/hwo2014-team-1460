#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import simulation.math2d as math2d
import numpy as np

class TrackSettings(object):
    def __init__(self, width, lanes):
        self.width = width
        self.lanes = lanes

class Track(object):

    def __init__(self, width = 10, lanes = [0.0]):
        self.pieces = []
        self.track_settings = TrackSettings(width, lanes)
    
    def add_start_point(self, position, angle, piece):
        assert len(self.pieces)==0, "Only one starting point is allowed"
        
        piece.position = position
        piece.tangent = math2d.rotate_vector_deg(piece.tangent, angle-90.0)
        self.pieces.append(piece)
        
    def add_piece(self, piece):
        assert len(self.pieces)>0, "A starting point needs to be added"
        piece.position = self.pieces[-1].get_end_position()
        piece.tangent = self.pieces[-1].get_end_tangent()
        
        self.pieces.append(piece)
    
    def calculate_distance(self, start, end, lanes, switch_database):
        start_idx, start_pos = start
        end_idx, end_pos = end
        if start_idx == end_idx:
            return end_pos - start_pos
        
        def calculate_piece(idx, start_pos, switch_pos):
            distance = 0.0
            start_lane = lanes[switch_pos]
            end_lane = start_lane
            piece = self.pieces[idx]
            if piece.is_switch:
                start_lane = lanes[switch_pos]
                switch_pos += 1
                end_lane = lanes[switch_pos]

            start_lane_offset = self.track_settings.lanes[start_lane]            
            if start_lane == end_lane:
                distance = piece.get_lane_length(start_lane_offset) - start_pos
            else:
                end_lane_offset = self.track_settings.lanes[end_lane]  
                distance = switch_database.get_length(piece, start_lane_offset, end_lane_offset)
                distance -= start_pos
            return (distance, switch_pos)
        
        distance, switch_pos = calculate_piece(start_idx, start_pos, 0)

        idx = start_idx + 1
        if end_idx<start_idx:
            while idx < len(self.pieces):
                offset, switch_pos = calculate_piece(idx, 0.0, switch_pos)
                distance+=offset
                idx+=1  
            idx = 0
        
        while idx < end_idx:
            offset, switch_pos = calculate_piece(idx, 0.0, switch_pos)
            distance+=offset
            idx+=1  
        
        return distance + end_pos
    
    def get_next_piece(self, piece):
        index = self.pieces.index(piece)
        if index < len(self.pieces)-1:
            return self.pieces[index + 1]
        else:
            return self.pieces[0]
