#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import numpy as np
import simulation.math2d as math2d

class TrackPiece(object):

    def __init__(self, length = None, radius = None, angle = None, switch = False, bridge = False):
        self.position = np.array((0.0, 0.0))
        self.tangent = np.array((1.0, 0.0))
        
        if length is not None:
            assert radius is None and angle is None, "Straights can not have angle or radius"
            self.length = length
            self.curve_radius = None
            self.curve_angle = None
        else:
            assert radius is not None and angle is not None, "Either length or radius+angle needed" 
            self.curve_radius = radius
            self.curve_angle = angle
            self.length = np.abs(2.0 * np.pi * radius * angle / 360.0)
        
        self.is_switch = switch
        self.switches = {}
        
    def init_lanes(self, lanes):
        #TODO should really make the lane function be able to use this information
        #currently it only initializes the switches
        def create_switch(lane1, lane1idx, lane2, lane2idx):
            print np.linspace(0.0, 1.0, num=3)
            start = self.get_lane(lane1, 0.0)
            end = self.get_lane(lane2, 1.0)
            mid1 =  self.get_lane(lane1, 0.5)
            mid2 = self.get_lane(lane2, 0.5)
            points=math2d.generate_bezier([start, mid1, mid2, end], 4)
            print points.shape
            length = 0
            xsum = 0
            ysum = 0
            for index,point in enumerate(points[:-1]):
                next=points[index+1]
                #print point, next
                v = next-point
                print np.sqrt(v.dot(v))
                xsum += v[0]
                ysum += v[1]
                length += np.sqrt(v.dot(v))

            print np.sqrt(xsum**2+ysum**2)
            print length
            self.switches[(lane1idx, lane2idx)] = length            
            #print points
            
        for index, lane in enumerate(lanes[:-1]):
            create_switch(lane, index, lanes[index+1], index+1)
            create_switch(lanes[index+1], index+1, lane, index)
        
    def is_straight(self):
        return self.curve_radius is None
    
    def is_curve(self):
        return not self.is_straight()
    
    def get_signed_curve_radius(self, lane_pos):
        curve_radius = 0.0
        if self.is_curve():
            curve_radius = self.curve_radius - lane_pos
            if self.curve_angle < 0:
                curve_radius = -self.curve_radius - lane_pos
            else:
                curve_radius = self.curve_radius - lane_pos
        return curve_radius
    
    def get_position(self, normalized_distance, distance_from_center = 0.0):
        if self.is_straight():
            normal = math2d.rotate_vector_deg(self.tangent, -90)
            center_adjust = -normal * distance_from_center
            
            return self.position + self.tangent * self.length * normalized_distance + center_adjust
        else:
            if self.curve_angle>0:
                normal = math2d.rotate_vector_deg(self.tangent, -90)
                center_adjust = -normal * distance_from_center
            else:
                normal = math2d.rotate_vector_deg(self.tangent, 90)
                center_adjust = normal * distance_from_center
            
            normal *= self.curve_radius
            return self.position - normal + math2d.rotate_vector_deg(normal + center_adjust, self.curve_angle * normalized_distance)
    
    def get_tangent(self, normalized_distance):
        if self.is_straight():
            return self.tangent
        else:
            return math2d.rotate_vector_deg(self.tangent, self.curve_angle*normalized_distance)
        
    def get_end_position(self):
        return self.get_position(1.0)
    
    def get_end_tangent(self):
        return self.get_tangent(1.0)

    def get_left_edge(self, track_width, normalized_distance):
        return self.get_position(normalized_distance, -track_width / 2.0)
    
    def get_right_edge(self, track_width, normalized_distance):
        return self.get_position(normalized_distance, track_width / 2.0)
    
    def get_lane(self, lane_pos, normalized_distance, distance_from_lane_center = 0.0):
        return self.get_position(normalized_distance, lane_pos + distance_from_lane_center)
    
    def get_lane_length(self, lane_pos):
        if self.is_straight():
            return self.length
        else:
            if self.curve_angle>0:
                radius = self.curve_radius - lane_pos
            else:
                radius = self.curve_radius + lane_pos
            lane_length = np.abs(2.0 * np.pi * radius * self.curve_angle / 360.0)
            return lane_length
        
    def get_switch_length(self, switch):
        return self.switches[switch]
    
    def distance_to_relative_for_lane(self, distance, lane_pos):
        return float(distance) / self.get_lane_length(lane_pos)
        
    def distance_to_relative(self, distance):
        return float(distance) / self.length
    