#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

from simulation.simulator_base import SimulatorBase
from simulation.switch_database import SwitchDatabase
import numpy as np
import copy

class Simulator(SimulatorBase):

    def __init__(self, track, cars, bots=[], switch_database = SwitchDatabase(), crash_duration = 0, external_simulator=None, turbo_delay=0):
        super(Simulator, self).__init__(track, cars, bots, switch_database)
        self.initialize_starting_positions()
        self.finished = False
        self.external_simulator = None
        self.turbo_delay_left = turbo_delay 
        self.turbo_delay_time = turbo_delay

        if external_simulator is not None:
            self.external_simulator = external_simulator
            for index, car in enumerate(self.cars):
                if index!=self.external_simulator[0]:
                    self.cars[index] = copy.deepcopy(self.external_simulator[1].cars[index])

        self.update_bots()
        self.tick_nr = 0
        
        self.slip_constants = (0.5, 0.5, 0.5, 0.5, 0.5)

        self.start_reaction_time = 0
        self.engine_power = 1.0
        self.drag = 0.0
        self.crash_duration = crash_duration
        

    def tick(self):
        if not self.finished:
            self.tick_nr += 1
            if self.turbo_delay_time:
                self.turbo_delay_left-=1
                #print self.turbo_delay_left
            if self.tick_nr > self.start_reaction_time:
                for index, car in enumerate(self.cars):
                    if self.external_simulator is not None and index!=self.external_simulator[0]:
                        self.cars[index] = copy.deepcopy(self.external_simulator[1].cars[index])
                    else:
                        if self.crash_duration > 0 and car.crash_duration > 0: 
                            car.crash_duration -= 1
                            car.throttle = 0.0
                            car.acceleration = 0.0
                        else:
                            if self.turbo_delay_left==0 and self.turbo_delay_time:
                                print "Turbo available"
                                car.turbo_available = (30, 3.0)
                            
                            c1, c2, c3, c4, c5 = self.slip_constants
                            current_piece = self.track.pieces[car.piece]
                            
                            if current_piece.is_switch and current_piece.is_curve() and car.lane!=car.end_lane:
                                lanes = self.track.track_settings.lanes
                                curve_radius = self.switch_database.get_curve_radius(current_piece, car.piece_distance, lanes[car.lane], lanes[car.end_lane])
                            else:
                                curve_radius = current_piece.get_signed_curve_radius(self.track.track_settings.lanes[car.lane])
                            if car.speed <= -(c5/c3)*np.sqrt(np.abs(curve_radius)):
                                curve_radius = 0
                            if curve_radius == 0:
                                a=c1*car.angle*car.speed + c2*car.slipspeed
                            elif curve_radius > 0:
                                a=((c1*car.angle+c5)*car.speed) + c2*car.slipspeed + c3/np.sqrt(curve_radius)*car.speed**2
                            else:
                                a=((c1*car.angle-c5)*car.speed) + c2*car.slipspeed - c3/np.sqrt(np.abs(curve_radius))*car.speed**2
                            car.slipacc = a
     
                            if car.turbo_duration > 0:
                                car.turbo_duration-=1
                            else:
                                car.turbo_factor = 1.0
                            
                            if car.turbo_activation is not None:
                                car.turbo_duration = car.turbo_activation[0]
                                car.turbo_factor = car.turbo_activation[1]
                                car.turbo_activation = None
                             
                            acceleration = car.speed + (car.turbo_factor*car.throttle / self.engine_power - car.speed * self.drag)
    
                            
                            #print "simulator", acceleration, car.speed, car.turbo_factor, car.throttle, self.engine_power, self.drag
                            #print acceleration, car.speed + (car.throttle / self.engine_power - car.speed * self.drag)
                            #acceleration = car.throttle * self.engine_power - self.drag * car.speed
                            car.acceleration = acceleration
                            car.speed = car.speed + acceleration
                            
                            if  car.turbo:
                                print "Activating turbo!!"
                                car.turbo = False
                                car.turbo_activation = car.turbo_available
                                car.turbo_available = None
                            
                            car.slipspeed = car.slipspeed + car.slipacc
                            car.angle = car.angle + car.slipspeed
                            
                            if np.abs(car.angle) >= 60.0 and self.crash_duration > 0:
                                print "Crashed with angle", car.angle
                                car.crash_duration = self.crash_duration
                                car.acceleration = -(car.speed-acceleration)
                                car.speed = 0.0
                                car.angle = 0.0
                                car.slipspeed = 0.0
                                car.throttle = 0.0
                            else:
                                car.distance = car.distance + car.speed
                                current_piece = self.track.pieces[car.piece]
                                
                                def calculate_left_in_piece(piece, distance):
                                    lanes=self.track.track_settings.lanes
                                    if car.lane == car.end_lane:
                                        return piece.get_lane_length(lanes[car.lane]) - distance
                                    else:
                                        return self.switch_database.get_length(piece, lanes[car.lane], lanes[car.end_lane]) - distance
                                    
                                #todo unit test this part
                                car.piece_distance+=car.speed
                                left_in_piece = calculate_left_in_piece(current_piece, car.piece_distance)
                                while left_in_piece < 0:
                                    car.piece += 1
                                    if car.piece == len(self.track.pieces):
                                        car.piece = 0
                                    current_piece = self.track.pieces[car.piece]
                                    if current_piece.is_switch:
                                        if car.switch!=0:
                                            #print "Activating switch", car.switch
                                            car.end_lane = car.lane + car.switch
                                            if car.end_lane >= len(self.track.track_settings.lanes):
                                                car.end_lane = car.lane
                                            elif car.end_lane<0:
                                                car.end_lane = 0
                                            car.switch = 0
                                    else:
                                        if car.lane != car.end_lane:
                                            car.lane = car.end_lane
                                    
                                    car.piece_distance = -left_in_piece
                                    left_in_piece = calculate_left_in_piece(current_piece, car.piece_distance)
                                
                                
                                self.update_car_position_from_piece_distance(car)        
                                self.update_car_angle_with_slip_angle(car, car.angle)
                self.update_bots()
                if self.turbo_delay_left == 0:
                    self.turbo_delay_left = self.turbo_delay_time
        return not self.finished
    
