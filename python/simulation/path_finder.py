#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import copy

class PathFinder(object):


    #TOOD:
    #Divide each piece in for example 10 segments
    #each segment has an optimal speed (median of your own speeds there)
    #your car has speedfactor 1
    #other cars has speedfactor <= 1
    #the other cars speedfactor is calculated by weigheted moving averaging the past N segments
    #a crashed car always has speedfactor 0
    #a crashed car doesn't prevent you from advancing, unless it's spawned
    #spawn is assumed to happen a few ticks before actually happening
    
    
    #then launch finders
    #advance all finders one tick at time
    #for a maximum of n ticks
    #if a switch is encountered, the finders can spawn itself
    #return the finder that advanced the furthest
    


    def __init__(self, track, switch_database):
        self.track = track
        self.switch_database = switch_database
        
    
    def find_path(self, start_piece, lane):
        pieces = self.track.pieces
        
        if start_piece == len(pieces) - 1:
            piece = 0
        else:
            piece = start_piece + 1
            
        next_piece = piece
        num_switches_found = 0
        
        end_piece = start_piece
        
        while piece!=start_piece and num_switches_found<5:
            p = pieces[piece]
            if p.is_switch:
                num_switches_found+=1
            piece = piece + 1
            if piece == len(pieces):
                piece = 0

        end_piece = piece        
        if num_switches_found == 0:
            return lane
        
        lanes = self.track.track_settings.lanes
        
           
        def append_switch(output, lane, switches):
            output.append(lane)
            if len(output)==num_switches_found+1:
                #print output
                switches.append(output)
            else:
                #print output
                append_switch(copy.copy(output), lane, switches)
                if lane + 1 <len(lanes):
                    append_switch(copy.copy(output), lane+1, switches)
                if lane-1 >= 0:
                    append_switch(copy.copy(output), lane-1, switches)
                    
        combinations = []
        append_switch([], lane, combinations)
        lengths = [(self.track.calculate_distance((next_piece, 0.0), (end_piece, 0.0), combination, self.switch_database), combination) for combination in combinations]
        lengths = sorted(lengths, key=lambda x: x[0])
        return lengths[0][1][1]