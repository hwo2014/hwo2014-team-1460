#==============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
import copy
import pkg_resources
from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from simulation.simulator import Simulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.switch_database import SwitchDatabase
from bots.jyckegarden import Jyckegarden
import settings

class TestRacePerformance(unittest.TestCase):

    def run_test(self, trackname, setting):
        switches = SwitchDatabase.load(pkg_resources.resource_filename("switches", "switches.bin"))
        
        with open(pkg_resources.resource_filename("tracks", trackname + ".txt"), "r") as f:
            jsonstring = f.read()
        
            track = TrackBuilder().build(jsonstring)
            cars = CarBuilder().build(jsonstring)
        
            source_simulator = Simulator(track = track, cars = cars, crash_duration = 400, switch_database = switches)
            
            
            source_simulator.engine_power = setting.engine_power
            source_simulator.drag = setting.drag
            source_simulator.slip_constants = setting.slipconstants
            simulator = AnalyzingSimulator(source_simulator, bots = [Jyckegarden()], switch_database = copy.deepcopy(switches))
            
            num_prewarm_laps = 2
            num_laps = 2

            prev_lap = simulator.cars[0].lap
            lap_time = 0
            while simulator.cars[0].lap <= num_prewarm_laps :
                simulator.tick()
                lap_time+=1
                if simulator.cars[0].lap!=prev_lap:
                    print trackname + " lap", prev_lap, lap_time, lap_time * (1.0 / 60.0)
                    prev_lap = simulator.cars[0].lap
                    lap_time = 0

            
            num_ticks = 0
            lap_time = 0
            while simulator.cars[0].lap <=  num_prewarm_laps + num_laps:
                simulator.tick()
                num_ticks+=1
                lap_time+=1
                if simulator.cars[0].lap!=prev_lap:
                    print trackname + " lap", prev_lap, lap_time, lap_time * (1.0 / 60.0)
                    prev_lap = simulator.cars[0].lap
                    lap_time = 0

            print num_ticks, num_ticks * (1.0 / 60.0)
            return num_ticks

    def test_keimola_default(self):
        self.assertEqual(882, self.run_test("keimola", settings.default))
            
    def test_france_default(self):
        self.assertEqual(879, self.run_test("france", settings.default))

    def test_germany_default(self):
        self.assertEqual(1057, self.run_test("germany", settings.default))

    def test_usa_default(self):
        self.assertEqual(601, self.run_test("usa", settings.default))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()