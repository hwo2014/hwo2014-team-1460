#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
from simulation.track_piece import TrackPiece
from simulation.track import TrackSettings
import numpy as np

class TrackPieceTest(unittest.TestCase):

    def test_create_straight(self):
        piece = TrackPiece(length=100)
        self.assertTrue(piece.is_straight())
        self.assertFalse(piece.is_curve())
        self.assertEqual(100, piece.length)
        np.testing.assert_almost_equal((0, 0), piece.position)
        np.testing.assert_almost_equal((1.0, 0.0), piece.tangent)
        np.testing.assert_almost_equal((100, 0), piece.get_end_position(), 3)
        np.testing.assert_almost_equal((1.0, 0), piece.get_end_tangent(), 3)
        np.testing.assert_almost_equal((50.0, 0), piece.get_position(0.5), 3)
        np.testing.assert_almost_equal((1.0, 0.0), piece.get_tangent(0.3), 3)
        self.assertAlmostEqual(100, piece.get_lane_length(5))

    def test_create_straight_with_different_length(self):
        piece = TrackPiece(length=10)
        self.assertEqual(10, piece.length)
        
    def test_create_right_curve(self):
        piece = TrackPiece(radius=200, angle=22.5)
        self.assertFalse(piece.is_straight())
        self.assertTrue(piece.is_curve())
        self.assertAlmostEqual(22.5, piece.curve_angle, 1)
        self.assertAlmostEqual(200, piece.curve_radius, 1)
        self.assertAlmostEqual(78.540, piece.length, 3)
        np.testing.assert_almost_equal((0, 0), piece.position)
        np.testing.assert_almost_equal((1.0, 0.0), piece.tangent)
        np.testing.assert_almost_equal((76.537, -15.224), piece.get_end_position(), 3)
        np.testing.assert_almost_equal((0.924, -0.383), piece.get_end_tangent(), 3)
        np.testing.assert_almost_equal((23.507,  -1.386), piece.get_position(0.3), 3)
        np.testing.assert_almost_equal((0.993, -0.117), piece.get_tangent(0.3), 3)
        self.assertAlmostEqual(76.576, piece.get_lane_length(5), 3)
        
    def test_create_left_curve(self):
        piece = TrackPiece(radius=200, angle=-22.5)
        self.assertFalse(piece.is_straight())
        self.assertTrue(piece.is_curve())
        self.assertAlmostEqual(-22.5, piece.curve_angle, 1)
        self.assertAlmostEqual(200, piece.curve_radius, 1)
        self.assertAlmostEqual(78.540, piece.length, 3)
        np.testing.assert_almost_equal((0, 0), piece.position)
        np.testing.assert_almost_equal((1.0, 0.0), piece.tangent)
        np.testing.assert_almost_equal((76.537, 15.224), piece.get_end_position(), 3)
        np.testing.assert_almost_equal((0.924, 0.383), piece.get_end_tangent(), 3)    
        np.testing.assert_almost_equal((0.993, 0.117), piece.get_tangent(0.3), 3)    
        
    def test_create_with_wrong_parameters(self):
        self.assertRaisesRegexp(AssertionError, "Either length or radius\\+angle needed", TrackPiece)
        self.assertRaisesRegexp(AssertionError, "Either length or radius\\+angle needed", TrackPiece, radius=20)
        self.assertRaisesRegexp(AssertionError, "Either length or radius\\+angle needed", TrackPiece, angle=200)
        self.assertRaisesRegexp(AssertionError, "Straights can not have angle or radius", TrackPiece, length = 10, radius=20)
        self.assertRaisesRegexp(AssertionError, "Straights can not have angle or radius", TrackPiece, length = 10, angle=20)
        self.assertRaisesRegexp(AssertionError, "Straights can not have angle or radius", TrackPiece, length = 10, angle=20, radius=200)
        
    def test_edge_positions_for_straight(self):
        piece = TrackPiece(length=100)
        piece.position = np.array((100, 100))
        piece.tangent = np.array((0, 1))
        np.testing.assert_almost_equal((90, 100), piece.get_left_edge(20, 0.0), 3)
        np.testing.assert_almost_equal((110, 100), piece.get_right_edge(20, 0.0), 3)
        np.testing.assert_almost_equal((90, 130), piece.get_left_edge(20, 0.3), 3)
        np.testing.assert_almost_equal((110, 130), piece.get_right_edge(20, 0.3), 3)
        
    def test_edge_positions_for_right_curve(self):
        piece = TrackPiece(radius = 200, angle=45)
        piece.position = np.array((100, 100))
        piece.tangent = np.array((0, 1))
        np.testing.assert_almost_equal((90, 100), piece.get_left_edge(20, 0.0), 3)
        np.testing.assert_almost_equal((110, 100), piece.get_right_edge(20, 0.0), 3)        
        np.testing.assert_almost_equal((95.802, 149.024), piece.get_left_edge(20, 0.3), 3)
        np.testing.assert_almost_equal((115.250, 144.355), piece.get_right_edge(20, 0.3), 3)
        
    def test_edge_positions_for_left_curve(self):
        piece = TrackPiece(radius = 200, angle=-45)
        piece.position = np.array((100, 100))
        piece.tangent = np.array((0, 1))
        np.testing.assert_almost_equal((90, 100), piece.get_left_edge(20, 0.0), 3)
        np.testing.assert_almost_equal((110, 100), piece.get_right_edge(20, 0.0), 3)
        np.testing.assert_almost_equal((84.750, 144.355), piece.get_left_edge(20, 0.3), 3)
        np.testing.assert_almost_equal((104.198, 149.024), piece.get_right_edge(20, 0.3), 3)
        
    def test_lane_positions_for_straight(self):
        piece = TrackPiece(length=100)
        piece.position = np.array((100, 100))
        piece.tangent = np.array((0, 1))
        np.testing.assert_almost_equal((105, 150), piece.get_lane(5, 0.5), 3)
        np.testing.assert_almost_equal((95, 200), piece.get_lane(-5, 1), 3)
        np.testing.assert_almost_equal((108, 150), piece.get_lane(5, 0.5, 3), 3)
    
    def test_distance_to_relative_straight(self):
        piece = TrackPiece(length=100)
        self.assertAlmostEqual(0.25, piece.distance_to_relative(25))
        self.assertAlmostEqual(0.25,piece.distance_to_relative_for_lane(25, -5))
        self.assertAlmostEqual(0.25,piece.distance_to_relative_for_lane(25, 5))
        
    def test_distance_to_relative_right_curve(self):
        piece = TrackPiece(radius = 200, angle = 22.5)
        self.assertAlmostEqual(0.5, piece.distance_to_relative(39.27), 3)
        self.assertAlmostEqual(0.488, piece.distance_to_relative_for_lane(39.27, -5), 3)
        self.assertAlmostEqual(0.513, piece.distance_to_relative_for_lane(39.27, 5), 3)
        
    def test_distance_to_relative_left_curve(self):
        piece = TrackPiece(radius = 200, angle = -22.5)
        self.assertAlmostEqual(0.5, piece.distance_to_relative(39.27), 3)
        self.assertAlmostEqual(0.513, piece.distance_to_relative_for_lane(39.27, -5), 3)
        self.assertAlmostEqual(0.488, piece.distance_to_relative_for_lane(39.27, 5), 3)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()