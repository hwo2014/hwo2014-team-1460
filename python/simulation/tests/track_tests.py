#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
from simulation.track import Track
from simulation.track_piece import TrackPiece
from simulation.switch_database import SwitchDatabase
import numpy as np

class TrackTest(unittest.TestCase):
    
    def test_create_empty_track(self):
        track = Track()
        self.assertEqual(0, len(track.pieces))

    def test_add_start_point(self):
        track = Track()
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        self.assertEqual(1, len(track.pieces))
        np.testing.assert_almost_equal((1, 0), track.pieces[0].tangent, 3)
        np.testing.assert_almost_equal((100, 100), track.pieces[0].position, 3)
        np.testing.assert_almost_equal((200, 100), track.pieces[0].get_end_position(), 3)
        np.testing.assert_almost_equal((1, 0), track.pieces[0].get_end_tangent(), 3)
        
    def test_two_starting_points_not_allowed(self):
        track = Track()
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        self.assertRaisesRegexp(AssertionError, "Only one starting point is allowed", track.add_start_point, (0, 0), 0, TrackPiece(length=100))
        
    def test_add_piece_to_track_without_starting_point_not_allowed(self):
        track = Track()
        self.assertRaises(AssertionError, track.add_piece, TrackPiece(length=200))
        
    def test_add_pice(self):
        track = Track()
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=10))
        track.add_piece(TrackPiece(length=20))
        self.assertEqual(2, len(track.pieces))
        np.testing.assert_almost_equal((110, 100), track.pieces[1].position, 0)
        np.testing.assert_almost_equal((130, 100), track.pieces[1].get_end_position(), 0)

    def test_add_curve_startpoint(self):
        track = Track()
        track.add_start_point(position = (100, 100), angle = -45, piece = TrackPiece(angle=45, radius = 100))
        np.testing.assert_almost_equal((-0.707, 0.707), track.pieces[0].tangent, 3)
        np.testing.assert_almost_equal((100, 100), track.pieces[0].position, 3)
        np.testing.assert_almost_equal((0, 1), track.pieces[0].get_end_tangent(), 3)
        np.testing.assert_almost_equal((70.710, 170.710), track.pieces[0].get_end_position(), 3)
        
    def test_complete_track_section(self):
        track = Track()
        track.add_start_point(position = (100, 100), angle = -45, piece = TrackPiece(angle=45, radius = 100))
        track.add_piece(TrackPiece(radius = 29.289, angle = 90))
        np.testing.assert_almost_equal((1, 0), track.pieces[1].get_end_tangent(), 3)
        np.testing.assert_almost_equal((100, 200), track.pieces[1].get_end_position(), 3)
        track.add_piece(TrackPiece(length = 100))
        np.testing.assert_almost_equal((1, 0), track.pieces[2].get_end_tangent(), 3)
        np.testing.assert_almost_equal((200, 200), track.pieces[2].get_end_position(), 3)
        track.add_piece(TrackPiece(radius = 100, angle = -180))
        np.testing.assert_almost_equal((-1, 0), track.pieces[3].get_end_tangent(), 3)
        np.testing.assert_almost_equal((200, 400), track.pieces[3].get_end_position(), 3)
        
    def test_calculate_distance(self):
        track = Track(width = 20, lanes = [5, -5])
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        track.add_piece(TrackPiece(length=200))
        track.add_piece(TrackPiece(angle = 45, radius = 100))
        track.add_piece(TrackPiece(length=100, switch=True))
        track.add_piece(TrackPiece(angle = -30, radius = 200))
        switch_database=SwitchDatabase()
        
        self.assertAlmostEqual(40, track.calculate_distance((0, 10), (0, 50), [0], switch_database))
        self.assertAlmostEqual(50, track.calculate_distance((0, 80), (1, 30), [0], switch_database))
        self.assertAlmostEqual(394.613, track.calculate_distance((0, 0), (3, 20), [0], switch_database), 3)
        self.assertAlmostEqual(217.338, track.calculate_distance((3, 10), (0, 20), [0, 0], switch_database), 3)
        self.assertAlmostEqual(317.338, track.calculate_distance((3, 10), (1, 20), [0, 0], switch_database), 3)
        self.assertAlmostEqual(100.499, track.calculate_distance((3, 0), (4, 0), [0, 1], switch_database), 3)
        self.assertAlmostEqual(74.613, track.calculate_distance((2, 20), (3, 20), [0, 1], switch_database), 3)
        self.assertAlmostEqual(74.613+80+10+0.499, track.calculate_distance((2, 20), (4, 10), [0, 1], switch_database), 3)
        self.assertAlmostEqual(287.213, track.calculate_distance((2, 10), (0, 20), [0, 1], switch_database), 3)
        
    def test_get_next_piece(self):
        track = Track(width = 20, lanes = [5])
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        track.add_piece(TrackPiece(length=200))
        track.add_piece(TrackPiece(angle = 45, radius = 100))
        
        self.assertEqual(track.pieces[1], track.get_next_piece(track.pieces[0]))
        self.assertEqual(track.pieces[2], track.get_next_piece(track.pieces[1]))
        self.assertEqual(track.pieces[0], track.get_next_piece(track.pieces[2]))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()