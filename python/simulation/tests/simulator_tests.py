#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
from simulation.simulator import Simulator
from simulation.track import Track
from simulation.track_piece import TrackPiece
from simulation.car import Car

import numpy as np

class SimulatorTest(unittest.TestCase):

    def test_simulator_construction(self):
        track = Track(width = 30, lanes = [-10, 0, 10])
        track.add_start_point(position=(100, 100), angle=90, piece=TrackPiece(length=100))
        cars = [Car("red", 20, 10, 3), Car("green", 20, 10, 3), Car("blue", 20, 10, 3)]
        simulator = Simulator(track = track, cars=cars)
        self.assertEqual(track, simulator.track)
        self.assertEqual(cars, simulator.cars)
        np.testing.assert_almost_equal((100, 110), cars[0].position, 3)
        np.testing.assert_almost_equal((100, 100), cars[1].position, 3)
        np.testing.assert_almost_equal((100, 90), cars[2].position, 3)
        np.testing.assert_almost_equal((1, 0), cars[0].direction)
        np.testing.assert_almost_equal((1, 0), track.pieces[0].tangent)

    def test_update_car_positons(self):
        track = Track(width = 30, lanes = [0])
        track.add_start_point(position=(100, 100), angle=90, piece=TrackPiece(length=100))
        track.add_piece(TrackPiece(angle=45, radius = 200))
        cars = [Car("red", 20, 10, 3)]
        simulator = Simulator(track = track, cars=cars)
        cars[0].piece = 0
        cars[0].piece_distance = 50
        simulator.update_car_position_from_piece_distance(cars[0])
        np.testing.assert_almost_equal((150, 100), cars[0].position, 3)
        cars[0].piece = 1
        cars[0].piece_distance = 0.0
        simulator.update_car_position_from_piece_distance(cars[0])
        np.testing.assert_almost_equal((200, 100), cars[0].position, 3)
        cars[0].piece = 1
        cars[0].piece_distance = 50
        simulator.update_car_position_from_piece_distance(cars[0])
        np.testing.assert_almost_equal((249.481, 93.782), cars[0].position, 3)

    def test_update_car_direction_with_slip_angle(self):
        track = Track(width = 30, lanes = [0])
        track.add_start_point(position=(100, 100), angle=90, piece=TrackPiece(length=100))
        track.add_piece(TrackPiece(angle=45, radius = 200))
        cars = [Car("red", 20, 10, 3)]
        simulator = Simulator(track = track, cars=cars)
        cars[0].piece = 0
        cars[0].piece_distance = 50
        simulator.update_car_angle_with_slip_angle(cars[0], 0.0)
        np.testing.assert_almost_equal((1, 0), cars[0].direction, 3)
        simulator.update_car_angle_with_slip_angle(cars[0], 45.0)
        np.testing.assert_almost_equal((0.707, -0.707), cars[0].direction, 3)
        cars[0].piece = 1
        cars[0].piece_distance = 50
        simulator.update_car_angle_with_slip_angle(cars[0], 0.0)
        np.testing.assert_almost_equal((0.969, -0.247), cars[0].direction, 3)
        
    def test_car_positons_are_on_the_driving_lane(self):
        track = Track(width = 30, lanes = [-10])
        track.add_start_point(position=(100, 100), angle=90, piece=TrackPiece(length=100))
        track.add_piece(TrackPiece(angle=45, radius = 200))
        cars = [Car("red", 20, 10, 3)]
        simulator = Simulator(track = track, cars=cars)
        cars[0].piece = 0
        cars[0].piece_distance = 50
        simulator.update_car_position_from_piece_distance(cars[0])
        np.testing.assert_almost_equal((150, 110), cars[0].position, 3)
        cars[0].piece = 1
        cars[0].piece_distance = 0.0
        simulator.update_car_position_from_piece_distance(cars[0])
        np.testing.assert_almost_equal((200, 110), cars[0].position, 3)
        cars[0].piece = 1
        cars[0].piece_distance = 50
        simulator.update_car_position_from_piece_distance(cars[0])
        np.testing.assert_almost_equal((249.529, 104.076), cars[0].position, 3)
        
    def test_calls_bot_on_every_tick_when_race_is_on(self):
        track = Track(width = 30, lanes = [-10, 10])
        track.add_start_point(position=(100, 100), angle=90, piece=TrackPiece(length=100))
        cars = [Car("red", 20, 10, 3), Car("blue", 20, 10, 3)]
        s = self
        class Bot:
            def __init__(self, car):
                self.car = car
                self.num_updates = 0
                self.command = None
            
            def update(self, car, simulator):
                s.assertEqual(cars[1], car)
                self.simulator = simulator
                self.num_updates+=1
            
        bots = [None, Bot(cars[1])]
            
        simulator = Simulator(track = track, cars=cars, bots = bots)
        self.assertEqual(1, bots[1].num_updates)
        self.assertEqual(simulator, bots[1].simulator)
        simulator.tick()
        self.assertEqual(2, bots[1].num_updates)
        simulator.finished = True
        simulator.tick()
        self.assertEqual(2, bots[1].num_updates)
        
    def test_runs_with_bot_output(self):
        track = Track(width = 30, lanes = [-10, 10])
        track.add_start_point(position=(100, 100), angle=90, piece=TrackPiece(length=100))
        track.add_piece(TrackPiece(length=100, switch=True))
        
        
        cars = [Car("red", 20, 10, 3)]
        class Bot:
            def __init__(self):
                output = [
                ("throttle", 1.0),
                ("switch", 1),
                ("throttle", 0.5),
                ("turbo", 1.0),
                ("throttle", 1.0),
                ("throttle", 1.0),
                ]
                self.output = iter(output)
            
            def update(self, car, simulator):
                self.command = self.output.next()
                
        bots = [Bot()]
            
        simulator = Simulator(track = track, cars=cars, bots = bots)
        self.assertEqual(1.0, simulator.cars[0].throttle)
        self.assertEqual(0, simulator.cars[0].switch)
        self.assertEqual(1.0, simulator.cars[0].turbo_factor)
        self.assertEqual(0, simulator.cars[0].turbo_duration)
        simulator.tick()
        self.assertEqual(1.0, simulator.cars[0].throttle)
        self.assertEqual(1, simulator.cars[0].switch)
        self.assertEqual(1.0, simulator.cars[0].turbo_factor)
        self.assertEqual(0, simulator.cars[0].turbo_duration)
        simulator.tick()
        self.assertEqual(0.5, simulator.cars[0].throttle)
        self.assertEqual(1, simulator.cars[0].switch)
        self.assertEqual(1.0, simulator.cars[0].turbo_factor)
        self.assertEqual(0, simulator.cars[0].turbo_duration)
        simulator.cars[0].turbo_available=(200, 2)
        simulator.tick()
        self.assertEqual(0.5, simulator.cars[0].throttle)
        self.assertEqual(1, simulator.cars[0].switch)
        self.assertEqual(1.0, simulator.cars[0].turbo_factor)
        self.assertEqual(0, simulator.cars[0].turbo_duration)
        simulator.tick()
        self.assertEqual(1.0, simulator.cars[0].throttle)
        self.assertEqual(1, simulator.cars[0].switch)
        self.assertEqual(1.0, simulator.cars[0].turbo_factor)
        self.assertEqual(0, simulator.cars[0].turbo_duration)
        simulator.tick()
        self.assertEqual(1.0, simulator.cars[0].throttle)
        self.assertEqual(1, simulator.cars[0].switch)
        self.assertEqual(2, simulator.cars[0].turbo_factor)
        self.assertEqual(200, simulator.cars[0].turbo_duration)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()