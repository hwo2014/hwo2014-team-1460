#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
import unittest
from simulation.track_builder import TrackBuilder
import numpy as np


class TrackBuilderTest(unittest.TestCase):


    def test_build_track(self):
        jsonstring = '{"msgType":"gameInit","data":{"race":{"track":{"pieces":[{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}}}}}'
        builder = TrackBuilder()
        track=builder.build(jsonstring)
        self.assertEqual(3, len(track.pieces))
        np.testing.assert_almost_equal((-300, -44), track.pieces[0].position, 3)
        np.testing.assert_almost_equal((1, 0), track.pieces[0].tangent, 3)
        np.testing.assert_almost_equal(100, track.pieces[2].curve_radius)
        np.testing.assert_almost_equal(45, track.pieces[2].curve_angle)
        self.assertEqual(2, len(track.track_settings.lanes))
        np.testing.assert_almost_equal(10, track.track_settings.lanes[1])
        np.testing.assert_almost_equal(40, track.track_settings.width)
        self.assertTrue(track.pieces[0].is_straight())
        self.assertFalse(track.pieces[0].is_switch)
        self.assertTrue(track.pieces[1].is_switch)
        self.assertFalse(track.pieces[2].is_switch)
        
    def test_build_track_with_multiple_lines(self):
        jsonstring ='{"data": {}, "msgType": "ping"}\n'
        jsonstring+='{"msgType":"yourCar","data":{"name":"fredizzimo","color":"red"},"gameId":"75410f69-23df-4e39-8eda-5bf80133021e"}\n' 
        jsonstring+='{"msgType":"gameInit","data":{"race":{"track":{"pieces":[{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}}}}}\n'
        jsonstring+='{"data": {}, "msgType": "ping"}'
        builder = TrackBuilder()
        track=builder.build(jsonstring)
        self.assertEqual(3, len(track.pieces))

    def test_build_track_only_first_found_is_used(self):
        jsonstring='{"msgType":"gameInit","data":{"race":{"track":{"pieces":[{"length":100.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}}}}}\n'
        jsonstring+='{"msgType":"gameInit","data":{"race":{"track":{"pieces":[{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}}}}}\n'
        builder = TrackBuilder()
        track=builder.build(jsonstring)
        self.assertEqual(1, len(track.pieces))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()