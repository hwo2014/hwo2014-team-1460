#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
import unittest
from simulation.track import Track
from simulation.track_piece import TrackPiece
from simulation.path_finder import PathFinder
from simulation.switch_database import SwitchDatabase

class PathFinderTest(unittest.TestCase):


    def test_simple_straight_returns0(self):
        track = Track(20, (-10, 10))
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        finder = PathFinder(track, SwitchDatabase())
        self.assertEqual(0, finder.find_path(0, 0))
        self.assertEqual(1, finder.find_path(0, 1))
        
    def test_simple_curve_returns0(self):
        track = Track(20, (-10, 10))
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(angle=45, radius=100))
        finder = PathFinder(track, SwitchDatabase())
        self.assertEqual(0, finder.find_path(0, 0))
        self.assertEqual(1, finder.find_path(0, 1))
        
    def test_track_with_one_switch_and_curve(self):
        track = Track(20, (-10, 10))
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        track.add_piece(TrackPiece(length=100, switch=True))
        track.add_piece(TrackPiece(angle=90, radius=100))
        finder = PathFinder(track, SwitchDatabase())
        self.assertEqual(1, finder.find_path(0, 0))
        self.assertEqual(1, finder.find_path(0, 1))
        
    def test_track_with_two_switches_and_three_lanes(self):
        track = Track(20, (-10, 0, 10))
        track.add_start_point(position = (100, 100), angle = 90, piece = TrackPiece(length=100))
        track.add_piece(TrackPiece(length=100, switch=True))
        track.add_piece(TrackPiece(angle=90, radius=100))
        track.add_piece(TrackPiece(length=100, switch=True))
        track.add_piece(TrackPiece(angle=-180, radius=100))
        finder = PathFinder(track, SwitchDatabase())
        self.assertEqual(1, finder.find_path(0, 0))
        self.assertEqual(1, finder.find_path(0, 1))  
        self.assertEqual(1, finder.find_path(0, 2))
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()