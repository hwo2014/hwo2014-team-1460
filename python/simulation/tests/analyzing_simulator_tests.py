#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
import unittest
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.simulator_base import SimulatorBase
from simulation.track import Track
from simulation.car import Car
from simulation.track_piece import TrackPiece
import pandas
import pandas.util.testing as pt

import numpy as np

class MockSimulator(SimulatorBase):
    def __init__(self):
        track = Track(width=40, lanes=[-10, 10])
        track.add_start_point((100, 100), 90, TrackPiece(length=100))
        track.add_piece(TrackPiece(angle = 90, radius = 200))
        cars = [
            Car(color="red", length=20, width=10, guide_position=3), 
            Car(color="blue", length=20, width=10, guide_position=3)
        ]
        
        super(MockSimulator, self).__init__(track, cars)
        #lane, piece, piece_distance, angle, throttle
        self.output = iter([
            [(0, 0, 0, 0.0, 0.0, 0.0),  (1, 1, 0, 0.0, 0.0, 0.0)],
            [(0, 0, 0, 20.0, 3.0, 1.0), (1, 0, 0, 30.0, 1.0, 0.5)],
            [(0, 1, 1, 10.0, 1.0, 1.0), (0, 0, 1, 10.0, 0.0, 0.3)],
            [(1, 1, 1, 20.0, 3.0, 0.5), (0, 0, 1, 30.0, 1.0, 1.0)]
        ])
        
        self.tick()        

        
    def tick(self):
        try:
            state = self.output.next()
            
            car_state = iter(state)
            for car in self.cars:
                lane, end_lane, piece, piece_distance, angle, throttle = car_state.next()
                car.lane = lane
                car.end_lane = end_lane
                car.piece = piece
                car.piece_distance = piece_distance
                car.angle = angle
                car.throttle = throttle
                car.turbo_factor = 1.0
                car.turbo_available = None
                car.turbo_duration = 0
                car.switch = 0
                car.turbo_activation = None
                car.turbo = False
                car.crash_duration = 0
                self.update_car_position_from_piece_distance(car)
                self.update_car_angle_with_slip_angle(car, angle)
            return True
        except StopIteration:
            return False
    
class AnalyzingSimulatorTest(unittest.TestCase):
    

    def test_construct(self):
        source = MockSimulator()
        simulator = AnalyzingSimulator(source)
        # Assume that the track and the cars are cloned
        self.assertEqual(source, simulator.source)
        self.assertNotEqual(source.track, simulator.track)
        self.assertEqual(source.track.track_settings.width, simulator.track.track_settings.width)
        self.assertNotEqual(source.cars, simulator.cars)
        self.assertEqual(len(source.cars), len(simulator.cars))
        self.assertEqual(source.cars[1].color, simulator.cars[1].color)
    
    def check_state(self, expected, actual):
        for car in xrange(2):
            e_car = expected.cars[car]
            a_car = actual.cars[car]
            np.testing.assert_almost_equal(e_car.position, a_car.position)
            np.testing.assert_almost_equal(e_car.direction, a_car.direction)
            self.assertEqual(e_car.lane, a_car.lane)
            self.assertEqual(e_car.angle, a_car.angle)
        
    def test_has_the_same_output_as_source(self):
        source = MockSimulator()
        # Note, not using the same instance, since, the analyzing simulator consumes the input 
        simulator = AnalyzingSimulator(MockSimulator())
        #sanity check
        np.testing.assert_almost_equal((100, 110), source.cars[0].position)
        np.testing.assert_almost_equal((100, 90), source.cars[1].position)
        self.check_state(source, simulator)
        source.tick()
        simulator.tick()
        self.check_state(source, simulator)
        
        #check simulation to the end
        while simulator.tick():
            source.tick()
        
        self.check_state(source, simulator)
        self.assertFalse(source.tick())
        
    def test_analyzation_of_past_frames(self):
        simulator = AnalyzingSimulator(MockSimulator())
        columns=["x", "y", "slipangle", "distance", "speed", "direction", "piece", "curve_radius", "lap", "throttle", "is_switch", "acceleration", "slipspeed", "slipacc", "start_lane", "end_lane", "piece_distance"]
        pt.assert_frame_equal(simulator.get_data_for_car(0), pandas.DataFrame([(100.0, 110.0, 0.0, 0.0, 0.0, 90.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)], index=[0], columns=columns))
        pt.assert_frame_equal(simulator.get_data_for_car(1), pandas.DataFrame([(100.0, 90.0, 0.0, 0.0, 0.0, 90.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0)], index=[0], columns=columns))
        simulator.tick()
        pt.assert_frame_equal(simulator.get_data_for_car(0), pandas.DataFrame([(100.0, 110.0, 0.0, 0.0, 0.0, 90.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), (120.0, 110.0, 3.0, 20.0, 20.0, 93.0, 0.0, 0.0, 1.0, 1.0, 0.0, 20.0, 3.0, 3.0, 0.0, 0.0, 20.0)], index=[0, 1], columns=columns))
        
      


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()