#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
from simulation.car import Car
import numpy as np


class CarTest(unittest.TestCase):

    def test_car_construction(self):
        car = Car(color="red", length=20, width=10, guide_position=3)
        self.assertEqual("red", car.color)
        np.testing.assert_almost_equal((0, 0), car.position)
        np.testing.assert_almost_equal((10, 20), car.dimensions)
        np.testing.assert_almost_equal((0, 1), car.direction)
        np.testing.assert_almost_equal(3, car.guide_position)
        np.testing.assert_almost_equal(((-5, 3), (5, 3), (5, -17), (-5, -17)), car.get_bounding_box())
    
    def test_translated_and_rotated_car_bounding_box(self):
        car = Car(color="red", length=20, width=10, guide_position=3)
        car.position = (100, 100)
        car.direction = (1, 0)
        np.testing.assert_almost_equal(((103, 105), (103, 95), (83, 95), (83, 105)), car.get_bounding_box())
        car.direction = (0, -1)
        np.testing.assert_almost_equal(((105, 97), (95, 97), (95, 117), (105, 117)), car.get_bounding_box())
        car.direction = (-1, 0)
        np.testing.assert_almost_equal(((97, 95), (97, 105), (117, 105), (117, 95)), car.get_bounding_box())

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()