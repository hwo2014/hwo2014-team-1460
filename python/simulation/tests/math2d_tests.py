#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
import math
import simulation.math2d as math2d
import numpy as np


class Test2dMath(unittest.TestCase):

    def test_rotation_matrix(self):
        np.testing.assert_equal(
            math2d.rotation_matrix_deg(180), 
            math2d.rotation_matrix_rad(np.pi)
        )
        np.testing.assert_almost_equal(
            math2d.rotation_matrix_deg(75), 
            np.matrix([[0.259, 0.966],[-0.966, 0.259]]), 
            3
        )
    
    def test_rotate_vector(self):
        np.testing.assert_equal(
            math2d.rotate_vector_deg((1, 0), 35), 
            math2d.rotate_vector_rad((1, 0), math.radians(35))
        )
        np.testing.assert_almost_equal(
            math2d.rotate_vector_deg((1, 0), 90), 
            (0, -1),
            3
        )
        
        np.testing.assert_almost_equal(
            math2d.rotate_vector_deg((1, 0), 120), 
            (-0.5, -0.866),
            3
        )
        
    def test_direction_to_angle(self):
        self.assertAlmostEqual(0.0, math2d.direction_to_angle_deg((0, 1), (0, 1)))
        self.assertAlmostEqual(0.0, math2d.direction_to_angle_deg((1, 0), (1, 0)))
        self.assertAlmostEqual(90.0, math2d.direction_to_angle_deg((1, 0), (0, 1)))
        self.assertAlmostEqual(180.0, math2d.direction_to_angle_deg((0, -1), (0, 1)))
        self.assertAlmostEqual(270.0, math2d.direction_to_angle_deg((-1, 0), (0, 1)))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()