#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import unittest
from simulation.car_builder import CarBuilder
import numpy as np


class Test(unittest.TestCase):

    def test_build_cars(self):
        jsonstring = '{"msgType":"gameInit","data":{"race":{"cars":[{"id":{"name":"fredizzimo","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}, {"id":{"name":"driver2","color":"blue"},"dimensions":{"length":41.0,"width":21.0,"guideFlagPosition":11.0}}]}}}'
        builder = CarBuilder()
        cars=builder.build(jsonstring)
        self.assertEqual(2, len(cars))
        self.assertEqual("red", cars[0].color)
        np.testing.assert_almost_equal((20, 40), cars[0].dimensions, 3)
        self.assertAlmostEqual(10.0, cars[0].guide_position)
        self.assertEqual("blue", cars[1].color)
        np.testing.assert_almost_equal((21, 41), cars[1].dimensions, 3)
        self.assertAlmostEqual(11, cars[1].guide_position)
        
    def test_build_cars_with_multiple_lines(self):
        jsonstring ='{"data": {}, "msgType": "ping"}\n'
        jsonstring+='{"msgType":"yourCar","data":{"name":"fredizzimo","color":"red"},"gameId":"75410f69-23df-4e39-8eda-5bf80133021e"}\n' 
        jsonstring+='{"msgType":"gameInit","data":{"race":{"cars":[{"id":{"name":"fredizzimo","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}]}}}\n'
        jsonstring+='{"data": {}, "msgType": "ping"}'
        builder = CarBuilder()
        cars=builder.build(jsonstring)
        self.assertEqual(1, len(cars))        

    def test_build_cars_only_first_found_is_used(self):
        jsonstring='{"msgType":"gameInit","data":{"race":{"cars":[{"id":{"name":"fredizzimo","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}, {"id":{"name":"driver2","color":"blue"},"dimensions":{"length":41.0,"width":21.0,"guideFlagPosition":11.0}}]}}}\n'
        jsonstring+='{"msgType":"gameInit","data":{"race":{"cars":[{"id":{"name":"fredizzimo","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}]}}}\n'
        builder = CarBuilder()
        cars=builder.build(jsonstring)
        self.assertEqual(2, len(cars))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()