#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================
import unittest

from simulation.switch_database import SwitchDatabase
from simulation.track_piece import TrackPiece

class SwitchDatabaseTest(unittest.TestCase):


    def test_default_for_straight(self):
        piece = TrackPiece(length=100, switch=True)
        database = SwitchDatabase()
        self.assertAlmostEqual(101.980390, database.get_length(piece, -10, 10), 6)

    def test_add_length_for_straight(self):
        piece = TrackPiece(length=100, switch=True)
        piece2 = TrackPiece(length=120, switch=True)
        database = SwitchDatabase()
        database.add_length(piece, -10, 10, 102.060275)
        self.assertAlmostEqual(102.060275, database.get_length(piece, -10, 10), 6)
        self.assertAlmostEqual(102.060275, database.get_length(piece, 10, -10), 6)
        self.assertAlmostEqual(102.060275, database.get_length(piece, 0, 20), 6)
        self.assertNotAlmostEqual(102.060275, database.get_length(piece2, 0, 20), 6)
        self.assertNotAlmostEqual(102.060275, database.get_length(piece, -20, 20), 6)

    def test_default_for_curve(self):
        piece = TrackPiece(radius=100, angle=90, switch=True)
        database = SwitchDatabase()
        self.assertAlmostEqual(142.126704, database.get_length(piece, -10, 10), 6)
        self.assertAlmostEqual(142.126704, database.get_length(piece, 10, -10), 6)

    def test_add_length_for_curve(self):
        piece = TrackPiece(radius=100, angle=90, switch=True)
        piece2 = TrackPiece(radius=110, angle=90, switch=True)
        piece3 = TrackPiece(radius=100, angle=45, switch=True)
        database = SwitchDatabase()
        database.add_length(piece, -10, 10, 150.0)
        self.assertAlmostEqual(150.0, database.get_length(piece, -10, 10))
        self.assertNotAlmostEqual(150.0, database.get_length(piece, 10, -10))
        self.assertNotAlmostEqual(150.0, database.get_length(piece2, -10, 10))
        self.assertNotAlmostEqual(150.0, database.get_length(piece3, -10, 10))
    
    def test_get_default_radius_for_curve(self):
        piece = TrackPiece(radius=100, angle=90, switch=True)
        database = SwitchDatabase()
        database.add_length(piece, -10, 10, 150.0)
        self.assertAlmostEqual(90, database.get_curve_radius(piece, 0.0, -10, 10))
        self.assertAlmostEqual(90, database.get_curve_radius(piece, 0.5, 10, -10))
        self.assertAlmostEqual(80, database.get_curve_radius(piece, 0.5, 20, -10))
    
    def _test_add_radius_for_curve(self):
        #disabled since the system has changed too much
        piece_right = TrackPiece(radius=100, angle=90, switch=True)
        piece_left = TrackPiece(radius=100, angle=-90, switch=True)
        database = SwitchDatabase()
        database.add_curve_radius(piece_right, 10, -10, 100.0, 105)
        database.add_curve_radius(piece_right, 10, -10, 30.0, 95)
        database.add_curve_radius(piece_left, -10, 10, 50.0, -100)
        database.add_curve_radius(piece_right, -10, 10, 100.0, 100)
        database.add_curve_radius(piece_left, 10, -10, 30.0, -95)
        database.add_curve_radius(piece_right, -10, 10, 50.0, 95)
        self.assertAlmostEqual(91.25, database.get_curve_radius(piece_right, 15.0, 10, -10))
        self.assertAlmostEqual(95, database.get_curve_radius(piece_right, 15.0, -10, 10))
        self.assertAlmostEqual(95, database.get_curve_radius(piece_right, 30.0, 10, -10))
        self.assertAlmostEqual(100, database.get_curve_radius(piece_right, 50.0, 10, -10))
        self.assertAlmostEqual(102.5, database.get_curve_radius(piece_right, 75.0, 10, -10))
        self.assertAlmostEqual(105, database.get_curve_radius(piece_right, 100.0, 10, -10))
        self.assertAlmostEqual(107.1063352, database.get_curve_radius(piece_right, (142.126704 + 100.0) / 2.0, 10, -10))
        
        self.assertAlmostEqual(-91.25, database.get_curve_radius(piece_left, 15.0, -10, 10))
        self.assertAlmostEqual(-95, database.get_curve_radius(piece_left, 15.0, 10, -10))
        self.assertAlmostEqual(-95, database.get_curve_radius(piece_left, 30.0, -10, 10))
        self.assertAlmostEqual(-100, database.get_curve_radius(piece_left, 50.0, -10, 10))
        self.assertAlmostEqual(-102.5, database.get_curve_radius(piece_left, 75.0, -10, 10))
        self.assertAlmostEqual(-105, database.get_curve_radius(piece_left, 100.0, -10, 10))
        self.assertAlmostEqual(-107.1063352, database.get_curve_radius(piece_left, (142.126704 + 100.0) / 2.0, -10, 10))
        pass
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()