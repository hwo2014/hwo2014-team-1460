#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import numpy as np
import math
import scipy
import cPickle


class SwitchDatabase(object):

    def __init__(self):
        self.straight_database = {}
        self.curve_database = {}
        self.curve_formula = {}
        self.curve_approximation = {}
        
    
    @staticmethod
    def f(x, a, b, c, d):
        return (1.0-x)**3*a+3*(1.0-x)**2*x*b+3*(1.0-x)*x**2*c+x**3*d
    
    def is_fully_known(self, piece, start_lane, end_lane):
        if piece.is_straight():
            return (piece.length, np.abs(start_lane - end_lane)) in self.straight_database
        else:
            key = self.get_curve_key(piece, start_lane, end_lane)
            return key in self.curve_formula 
    
    def get_length(self, piece, start_lane, end_lane):
        if piece.is_straight():
            if (piece.length, np.abs(start_lane - end_lane)) in self.straight_database:
                return self.straight_database[(piece.length, np.abs(start_lane - end_lane))]
            
            return np.sqrt(piece.length**2 + (start_lane - end_lane)**2)
        else:
            key=self.get_curve_key(piece, start_lane, end_lane)
            if key in self.curve_database:
                length = self.curve_database[key][0]
                if length != 0.0:
                    return length
            
            start_pos = np.array(piece.get_position(0.0, start_lane))
            end_pos = np.array(piece.get_position(1.0, end_lane))
            v = end_pos - start_pos
            return np.sqrt(v.dot(v))

    def get_curve_key(self, piece, start_lane, end_lane):
        return (piece.curve_radius, piece.curve_angle, start_lane, end_lane)
    
    def calculate(self, piece, start_lane, end_lane):
        key = self.get_curve_key(piece, start_lane, end_lane)
        v = self.curve_database[key]
        distance_radius=v[1]
        if len(distance_radius)>5:
            radiuses = np.array(distance_radius.values())
            x = np.array(distance_radius.keys())
            popt, pcov = scipy.optimize.curve_fit(self.f, x, radiuses)
            a, b, c, d=popt
            #print "Optimized curve", key, popt, pcov, len(x), v[0]
            self.curve_formula[key]=(a, b, c, d)
            try:
                del self.curve_approximation[key]
            except:
                pass
    
    def add_length(self, piece, start_lane, end_lane, length):
        if piece.is_straight():
            self.straight_database[(piece.length, np.abs(start_lane-end_lane))] = length
        else:
            key = self.get_curve_key(piece, start_lane, end_lane)

            #print key, (piece.curve_radius, piece.curve_angle, start_lane, end_lane)
            distance_angles = self.curve_database.setdefault(key, [0.0, dict()])
            if distance_angles[0]==0.0:
                distance_angles[1] = {np.floor((distance / length)*100.0)/100.0: radius for (distance, radius) in distance_angles[1].items()}
                distance_angles[0]=length
                print "Adding switch length"
                self.calculate(piece, start_lane, end_lane)
            else:
                print "Switch length does not match"
            
            
    def get_curve_radius(self, piece, piece_distance, start_lane, end_lane):
        
        def calculate_angle_with_formula(length, a, b, c, d):
            x = piece_distance / length
            x=np.floor(x*100.0)/100.0
            return self.f(x, a, b, c, d)
        
        key = self.get_curve_key(piece, start_lane, end_lane)
        #self.calculate(piece, start_lane, end_lane)
        if key in self.curve_formula:
            length = self.curve_database[key][0]
            return calculate_angle_with_formula(length, *self.curve_formula[key])


        if key in self.curve_approximation:
            #print "Using approximation"
            approximation = self.curve_approximation[key]
            index = int(math.floor(piece_distance))
            radius = approximation[index]
            return radius
        else:
            #print "Using no approximation at all"
            if piece.curve_angle > 0:
                return piece.curve_radius - np.max(np.abs((start_lane, end_lane)))
            else:
                return -piece.curve_radius + np.max(np.abs((start_lane, end_lane)))
    
    def add_curve_radius(self, piece, start_lane, end_lane, piece_distance, radius, min_radius=False):
        key = self.get_curve_key(piece, start_lane, end_lane)

        if not min_radius:
            distance_angles = self.curve_database.setdefault(key, [0.0, dict()])
            if distance_angles[0]==0.0:
                distance_angles[1][piece_distance] = radius
            else:
                length = distance_angles[0]
                x = piece_distance / length
                x=np.floor(x*100.0)/100.0
                if not x in distance_angles[1]:
                    distance_angles[1][x] = radius
                    self.calculate(piece, start_lane, end_lane)
            
        if key in self.curve_formula:
            return

        if not key in self.curve_approximation:
            length = piece.length
            approximation = np.empty(length*2)
            if piece.curve_angle > 0:
                approximation.fill(piece.curve_radius - np.max(np.abs((start_lane, end_lane))))
            else:
                approximation.fill(-piece.curve_radius + np.max(np.abs((start_lane, end_lane))))
            self.curve_approximation[key]=approximation
        else:
            approximation = self.curve_approximation[key]
        index = int(math.floor(piece_distance))
        
        if min_radius and np.abs(approximation[index]) > np.abs(radius):
            return
        
        width = int(round(piece.length / 3.0)) 
        first_index = index - width / 2
        last_index = index + width / 2
        
        if first_index < 0:
            first_index = 0
        if last_index >= len(approximation):
            last_index = len(approximation) - 1
            
        for x in xrange(first_index, last_index + 1):
            x_diff = np.abs(index - x)
            multiplier = 1.0-x_diff / (width + 1.0)
            value_diff = radius - approximation[x]
            approximation[x]+=value_diff * multiplier
        
        
    
    def save(self, filename):
        with open(filename, "wb") as f:
            cPickle.dump(self, f, 2)
    
    @staticmethod
    def load(filename):
        try:
            with open(filename, "rb") as f:
                ret = cPickle.load(f)
                return ret
        except Exception as e:
            print "Error"
            print e
            return SwitchDatabase()
