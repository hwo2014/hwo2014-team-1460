#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import copy
from simulation.simulator_base import SimulatorBase
from simulation.switch_database import SwitchDatabase
from simulation.timer import Timer
from simulation.replay_simulator import ReplaySimulator
import math2d
import pandas
import scipy.optimize as optimize
import numpy as np
import random

#import matplotlib.pyplot as plt

class AnalyzingSimulator(SimulatorBase):

    def __init__(self, simulator_to_analyze, switch_database = SwitchDatabase(), car_to_analyze = 0, bots=[]):
        self.source = simulator_to_analyze
        super(AnalyzingSimulator, self).__init__(copy.deepcopy(simulator_to_analyze.track), copy.deepcopy(simulator_to_analyze.cars), switch_database=switch_database, bots=bots)
        num_cars = len(self.cars)
        columns = ["x", "y", "slipangle", "distance", "speed", "direction", "piece", "curve_radius", "lap", "throttle", "is_switch", "acceleration", "slipspeed", "slipacc", "start_lane", "end_lane", "piece_distance", "is_close"]
        self.acceleration_column = columns.index("acceleration")
        self.slipspeed_column = columns.index("slipspeed")
        self.slipacc_column = columns.index("slipacc")
        self.speed_column = columns.index("speed")
        self.slipangle_column = columns.index("slipangle")
        self.distance_column = columns.index("distance")
        self.throttle_column = columns.index("throttle")
        self.curve_radius_column = columns.index("curve_radius")
        self.piece_column = columns.index("piece")
        self.start_lane_column = columns.index("start_lane")
        self.end_lane_column = columns.index("end_lane")
        self.piece_distance_column = columns.index("piece_distance")
        self.is_close_column = columns.index("is_close")
        self.columns = columns
        max_to_analyze = 100000
        self.data = [np.empty([max_to_analyze,len(columns)], dtype=np.float64) for _ in xrange(num_cars)]
        self.prev_piece_pos = [None]*num_cars
        self.engine_power = 0.0
        self.drag = 0.0
        self.car_to_analyze = car_to_analyze
        
        self.slipconstants = (0.0, 0.0, 0.0, 0.0, 0.0)
        
        self.slipconstants_found = False
        self.highestc4 = 0.0
        self.lowestc4 = 1.0

        self.tick_nr = 0
        self.start_reaction_time = -1
        
        for car in self.cars:
            car.distance = 0.0
            car.lap = 1
        self.update_cars_from_source()
        self.update_bots()
        self.update_bot_output_to_source_cars()

    
    def tick(self):
        if self.source.tick():
            self.tick_nr += 1
            self.update_cars_from_source()
            self.update_bots()
            self.update_bot_output_to_source_cars()
            return True
        else:
            return False
        
    def update_cars_from_source(self):
        for index, car in enumerate(self.cars):
            source_car = self.source.cars[index]
            car.position = source_car.position
            car.direction = source_car.direction
            car.lane = source_car.lane
            car.end_lane = source_car.end_lane
            car.angle = source_car.angle
            car.throttle = source_car.throttle
            car.turbo_factor = source_car.turbo_factor
            car.turbo_available = source_car.turbo_available
            car.turbo_duration = source_car.turbo_duration
            car.turbo_activation = source_car.turbo_activation
            car.turbo = source_car.turbo
            car.switch = source_car.switch
            car.crash_duration = source_car.crash_duration
            
            if car.piece > source_car.piece:
                car.lap = car.lap + 1
            
            car.piece = source_car.piece
            car.piece_distance = source_car.piece_distance
            if self.prev_piece_pos[index] == None:
                self.prev_piece_pos[index] = (car.lane, car.piece, car.piece_distance) 
            self.append_data(index)
            self.prev_piece_pos[index] = (car.lane, car.piece, car.piece_distance)
            
    def update_bot_output_to_source_cars(self):
        if type(self.source) != ReplaySimulator:
            for index, car in enumerate(self.cars):
                source_car = self.source.cars[index]
                source_car.throttle = car.throttle
                source_car.switch = car.switch
                source_car.turbo = car.turbo
                
    def tick_until_done(self):
        while self.tick():
            pass
        
    def get_current_tick_nr(self):
        return self.tick_nr
    
    def append_data(self, car_index):
        car = self.cars[car_index]
       
        prev_lane, prev_piece, prev_piece_distance = self.prev_piece_pos[car_index]
        if car.piece == prev_piece:
            start_lane = car.lane
            end_lane = car.end_lane
        else:
            start_lane = prev_lane
            end_lane = car.lane

        other_cars_close = False
        if self.tick_nr > 0:            
            cur_lane1 = self.data[car_index][self.tick_nr-1, self.start_lane_column]
            cur_lane2 = self.data[car_index][self.tick_nr-1, self.end_lane_column]
            cur_piece = self.data[car_index][self.tick_nr-1, self.piece_column]
            #cur_distance = self.data[car_index][self.tick_nr-1, self.distance_column]

            for index, _ in enumerate(self.cars):
                if index != car_index:
                    lane1 = self.data[index][self.tick_nr-1, self.start_lane_column]
                    lane2 = self.data[index][self.tick_nr-1, self.end_lane_column]
                    piece = self.data[index][self.tick_nr-1, self.piece_column]
                    if cur_lane1 == lane1 or cur_lane2 == lane2 or cur_lane2 == lane1 or cur_lane2 == lane2:
                        if piece==len(self.track.pieces)-1 and cur_piece<piece:
                            piece=-1
                        if cur_piece==len(self.track.pieces) and piece<cur_piece:
                            piece+=len(self.track.pieces)
                        if car_index == self.car_to_analyze:
                            print cur_piece, piece, index
                        
                        if piece==cur_piece or piece==cur_piece+1 or piece==cur_piece-1:
                            other_cars_close = True

        #if car_index==self.car_to_analyze:
        #    print other_cars_close, car_index
        #print start_lane, end_lane, car.piece, prev_piece
        speed = self.track.calculate_distance((prev_piece, prev_piece_distance), (car.piece, car.piece_distance), [start_lane, end_lane], self.switch_database)    
        if self.engine_power!=0.0 and self.drag!=0.0 and car_index == self.car_to_analyze and not other_cars_close and car.crash_duration==0:    
            old = self.data[car_index][self.tick_nr-1]
            prev_speed = old[self.speed_column]
            predicted_acceleration = prev_speed + (car.turbo_factor*car.throttle / self.engine_power - prev_speed * self.drag)
            #print "analyzer", type(self.source), predicted_acceleration, prev_speed, car.turbo_factor, car.throttle, self.engine_power, self.drag
            predicted_speed = prev_speed + predicted_acceleration
            if not np.isclose(speed, predicted_speed, rtol=0.000001):
                speed_diff = predicted_speed - speed
                prev_piece = self.track.pieces[int(old[self.piece_column])]
                #print "predicted speed is wrong!!", old[self.throttle_column], self.data[car_index][self.tick_nr-2, self.throttle_column], prev_piece.is_switch and start_lane!=end_lane
                
                if prev_piece.is_switch and start_lane!=end_lane:
                    lanes = self.track.track_settings.lanes
                    speed = predicted_speed
                    start_lane_offset = lanes[start_lane]
                    end_lane_offset = lanes[end_lane]
                    old_length = self.switch_database.get_length(prev_piece, start_lane_offset, end_lane_offset)
                    real_length = old_length +speed_diff
                    print "Adding switch", speed_diff, car.turbo_factor, real_length, old_length
                    
                    self.switch_database.add_length(prev_piece, start_lane_offset, end_lane_offset, real_length)
        if self.slipconstants_found and car_index == self.car_to_analyze  and not other_cars_close and car.crash_duration==0:
            old = self.data[car_index][self.tick_nr-2]
            prev_piece = self.track.pieces[int(old[self.piece_column])]
            old_start_lane = int(old[self.start_lane_column])
            old_end_lane = int(old[self.end_lane_column])
            if prev_piece.is_switch and old_start_lane!=old_end_lane:
                c1, c2, c3, c4, c5 = self.slipconstants
                found_radius = self.curve_radius_f(self.data[car_index], self.tick_nr-2, self.tick_nr-2, c1, c2, c3, c4, c5)[0]
                if prev_piece.is_curve():
                    piece_distance = old[self.piece_distance_column]
                    lanes = self.track.track_settings.lanes
                    start_lane_offset = lanes[old_start_lane]
                    end_lane_offset = lanes[old_end_lane]
                    if not np.isclose(0.0, found_radius):
                        self.switch_database.add_curve_radius(prev_piece, start_lane_offset, end_lane_offset, piece_distance, found_radius)
                    else:
                        found_radius = 0
                        prev_speed = old[self.speed_column]
                        curve_radius = (prev_speed/(-c5/c3))**2
                        curve_radius+=1
                        if prev_piece.curve_angle < 0:
                            curve_radius = -curve_radius
                        self.switch_database.add_curve_radius(prev_piece, start_lane_offset, end_lane_offset, piece_distance, curve_radius, True)
                        #print "No slipping", prev_piece.curve_radius, prev_piece.curve_angle, start_lane_offset, end_lane_offset, curve_radius
                    
        
        car.distance+=speed
        piece = self.track.pieces[car.piece]
        curve_radius = piece.get_signed_curve_radius(self.track.track_settings.lanes[car.lane])
        df = self.data[car_index]

        car.speed = speed
        
        df[self.tick_nr] = [
                                car.position[0], 
                                car.position[1], 
                                car.angle, 
                                car.distance, 
                                speed, 
                                math2d.direction_to_angle_deg(car.direction, (0, 1)), 
                                car.piece, 
                                curve_radius, 
                                car.lap,
                                car.throttle,
                                piece.is_switch,
                                0.0,
                                0.0,
                                0.0,
                                car.lane,
                                car.end_lane,
                                car.piece_distance,
                                other_cars_close
                            ]
        
        if self.tick_nr > 0:
            current_tick = self.tick_nr
            last_tick = self.tick_nr - 1
            acceleration = df[current_tick, self.speed_column] - df[last_tick, self.speed_column]
            slipspeed = df[current_tick, self.slipangle_column] - df[last_tick, self.slipangle_column]
            slipacc = slipspeed - df[last_tick, self.slipspeed_column]
            df[current_tick, self.acceleration_column] = acceleration
            df[current_tick, self.slipspeed_column] = slipspeed
            df[current_tick, self.slipacc_column] = slipacc
            
            car.slipspeed = slipspeed
            car.slipacc = slipacc
            car.acceleration = acceleration
        else:
            car.slipspeed = 0.0
            car.slipacc = 0.0
            car.acceleration = 0.0

        if car_index == self.car_to_analyze:      
            if self.start_reaction_time == -1:
                if speed>0:
                    self.start_reaction_time = self.tick_nr-1
            elif self.engine_power == 0 or self.drag == 0 and not other_cars_close:
                self.engine_power, self.drag = self._find_engine_power_and_drag(car_index)
            elif not other_cars_close:
                self.slipconstants = self.find_slipping_coefficients(car_index)
        
        
    def get_data_for_car(self, car):
        return pandas.DataFrame(self.data[car][0:self.tick_nr+1], columns = self.columns, index=[x for x in range(self.tick_nr+1)])
    
    def find_start_reaction_time(self, car):
        return self.start_reaction_time
    
    def get_engine_power_and_drag(self):
        return (self.engine_power, self.drag)
    
    def get_slipping_coefficients(self):
        return tuple(self.slipconstants)
    
    def _find_engine_power_and_drag(self, car):
        num_frames_needed = 5
        if self.tick_nr < num_frames_needed:
            return (0.0, 0.0)
        
        df = self.data[car]
        
        for i in xrange(num_frames_needed):
            if(df[self.tick_nr-i][self.acceleration_column]) == 0.0:
                return (0.0, 0.0)
            if df[self.tick_nr-i][self.is_close_column] == True:
                return (0.0, 0.0)            
        
        def f(start, end, engine_power, drag):
            cf = df[start]
            distance = cf[self.distance_column]
            speed = cf[self.speed_column]
            acceleration = cf[self.acceleration_column]
            ret = [distance]
            for i in xrange(start+1, end+1):
                cf = df[i-self.start_reaction_time]
                throttle = cf[self.throttle_column]
                acceleration=speed + (throttle / engine_power - speed * drag)
                speed = speed + acceleration
                distance = distance + speed
                ret.append(distance)
            return ret
        
        def f2(indices, engine_power, drag):
            start = indices[0]
            end = indices[-1]
            results = f(start, end, engine_power, drag)
            return [results[x-start] for x in indices]

        start = self.tick_nr-(num_frames_needed -1)
        end = self.tick_nr
        x = np.array([x for x in xrange(start, end+1)])
        y = df[start:end+1, self.distance_column]
          
        results = optimize.curve_fit(f2, x, y)
        print "Engine constants"
        print results
        return tuple(results[0])
    
    def find_engine_power_and_drag_legay(self, car):
        df = self.get_data_for_car(car)
        
        start_reaction = self.find_start_reaction_time(car)
        
        def f(start, end, engine_power, drag):
            cf = df.ix[start]
            distance = cf.distance
            speed = cf.speed
            acceleration = cf.acceleration
            ret = [distance]
            for i in xrange(start+1, end+1):
                cf = df.ix[i-start_reaction]
                throttle = cf.throttle
                acceleration=speed + (throttle / engine_power - speed * drag)
                speed = speed + acceleration
                distance = distance + speed
                ret.append(distance)
            return ret
        
        def f2(indices, engine_power, drag):
            start = indices[0]
            end = indices[-1]
            results = f(start, end, engine_power, drag)
            return [results[x-start] for x in indices]

        start = start_reaction + 2
        end = start + 100
        x =  np.array(df.index.values[start:end])
        y = np.array((df.distance).tolist()[start:end])   
        results = optimize.curve_fit(f2, x, y)
        print "Engine constants"
        print results
        return tuple(results[0])
    
    def slip_f(self, df, start, end, c1, c2, c3, c4, c5):
        results = []
        for i in xrange(start, end+1):
            old = df[i-1]
           
            curve_radius = old[self.curve_radius_column]
            speed = old[self.speed_column]
            slipangle = old[self.slipangle_column]
            slipspeed = old[self.slipspeed_column]
            
            if speed**2 < np.abs(curve_radius*c4):
                curve_radius = 0

            if curve_radius == 0:
                a=c1*slipangle*speed + c2*slipspeed
            elif curve_radius > 0:
                a=((c1*slipangle+c5)*speed) + c2*slipspeed + c3/np.sqrt(curve_radius)*speed**2
            else:
                a=((c1*slipangle-c5)*speed) + c2*slipspeed - c3/np.sqrt(np.abs(curve_radius))*speed**2
            
            results.append(a)
        
        return results
    
    def curve_radius_f(self, df, start, end, c1, c2, c3, c4, c5):
        results = []
        for i in xrange(start, end+1):
            if i+1>=self.tick_nr:
                results.append(0.0)
            else:
                speed = df[i, self.speed_column]
                slipangle = df[i, self.slipangle_column]
                slipspeed = df[i, self.slipspeed_column]
                actual_a = df[i+1, self.slipacc_column]
                a=c1*slipangle*speed + c2*slipspeed
                radius = 0.0
                if np.isclose(a, actual_a, rtol=0.0001):
                    radius = 0.0
                else:
                    radius1 = (c3*speed**2 / (actual_a - ((c1*slipangle+c5)*speed) - c2*slipspeed))**2
                    radius2 = (-c3*speed**2 / (actual_a - ((c1*slipangle-c5)*speed) - c2*slipspeed))**2
                    
                    if speed**2 > np.abs(radius1*c4): #todo fix this to not depend on c4
                        radius = radius1
                    else:
                        radius = -radius2
                    
                    #print radius, df[i][self.curve_radius_column]
            
                results.append(radius)
        
        return results        
    
    def find_slipping_coefficients(self, car):
        if self.tick_nr<3:
            return self.slipconstants
        
        df = self.data[car]
        oldc1, oldc2, oldc3, oldc4, oldc5 = self.slipconstants
        c1 = oldc1
        c2 = oldc2
        c3 = oldc3
        c4 = oldc4
        c5 = oldc5
        
        current_df = df[self.tick_nr]
        old_df = df[self.tick_nr - 1]
        
        if old_df[self.is_close_column]==True:
            return self.slipconstants
        
        
        slipacc = current_df[self.slipacc_column]
        
        num_frames_needed = 10
        
        if np.isclose(old_df[self.curve_radius_column], 0.0):
            return self.slipconstants
        
        if old_df[self.start_lane_column]!=old_df[self.end_lane_column]:
            return self.slipconstants
        c4_tol = 0.0000001
        
        if oldc4==0.0:
            if not np.isclose(slipacc, 0.0, rtol=0.001) and not np.isclose(df[self.tick_nr - 2, self.curve_radius_column], 0):
                self.highestc4 = old_df[self.speed_column]**2/np.abs(old_df[self.curve_radius_column])
                self.lowestc4 = df[self.tick_nr - 2, self.speed_column]**2/np.abs(df[self.tick_nr - 2, self.curve_radius_column])
                c4=self.highestc4
                print "Initial slip limit", c4
        elif self.slipconstants_found:
            slipacc = current_df[self.slipacc_column]
            under = self.slip_f(df, self.tick_nr, self.tick_nr, c1, c2, c3, 10000, c5)[0]
            over = self.slip_f(df, self.tick_nr, self.tick_nr, c1, c2, c3, 0, c5)[0]
            current_c4 = old_df[self.speed_column]**2/np.abs(old_df[self.curve_radius_column])
            if current_c4 < self.highestc4:
                if np.isclose(over, slipacc, rtol=0.001) and not np.isclose(under, slipacc, rtol=0.001):
                    self.highestc4 = current_c4
            elif current_c4 > self.lowestc4:
                if np.isclose(under, slipacc, rtol=0.001) and not np.isclose(over, slipacc, rtol=0.001):
                    self.lowestc4 = current_c4

            c4=(self.highestc4+self.lowestc4) / 2.0

        elif self.tick_nr > num_frames_needed+1:
            for i in xrange(num_frames_needed):
                old = df[self.tick_nr-i-1]
                if np.isclose(old[self.curve_radius_column], 0.0):
                    return self.slipconstants
                if old[self.speed_column]**2/np.abs(old[self.curve_radius_column]) < c4:
                    return self.slipconstants
                if np.isclose(old[self.acceleration_column], 0.0, rtol=0.001):
                    return self.slipconstants
                if old[self.is_close_column] == True:
                    return self.slipconstants
                
            def f2(indices, c1, c2, c3, c5):
                start = indices[0]
                end = indices[-1]
                results = self.slip_f(df, start, end, c1, c2, c3, c4, c5)
                return [results[x-start] for x in indices]
            
            start = self.tick_nr-(num_frames_needed -1)
            end = self.tick_nr
            
            x = np.array([x for x in xrange(start, end+1)])
            y = df[start:end+1, self.slipacc_column]
            results = optimize.curve_fit(f2, x, y)
            
            self.slipconstants_found = True 
            c1, c2, c3, c5 = results[0]
            print "Final results"
            print results
            print self.lowestc4
            print self.highestc4
            print c1, c2, c3, c4, c5
            if np.abs(results[0][1]) < 0.000000000001:
                self.reset_slip_constants()
                return self.slipconstants
            if np.abs(results[0][2]) < 0.000000000001:
                self.reset_slip_constants()
                return self.slipconstants

        
        return (c1, c2, c3, c4, c5)
    
    def reset_slip_constants(self):
        self.slip_constants = (0.0, 0.0, 0.0, 0.0, 0.0)
        self.lowestc4 = 0.0
        self.highestc4 = 0.0
        self.slipconstants_found = False

        
    def plot_current(self, car):
        df = self.data[car]
        c1, c2, c3, c4, c5 = self.slipconstants
        print c1, c2, c3, c4, c5
        print self.lowestc4
        print self.highestc4
        x = [x for x in range(0, self.tick_nr+1)]
        plt.plot(x, df[:self.tick_nr+1, self.slipacc_column], label="actual")
        plt.plot(x, self.slip_f(df, 0, self.tick_nr, c1, c2, c3, c4, c5), "--", label="predicted")
        plt.plot(x, df[:self.tick_nr+1, self.curve_radius_column] / 1000.0, label="curve_radius")
        plt.plot(x, df[:self.tick_nr+1, self.speed_column] / 50.0, label="speed")
        plt.plot(x, df[:self.tick_nr+1, self.slipangle_column] / 100.0, label="slipangle")
        plt.plot(x, df[:self.tick_nr+1, self.slipspeed_column] / 10.0, label="slipspeed")
        plt.plot(x, df[:self.tick_nr+1, self.throttle_column] / 10.0, label="throttle")
        plt.plot(x, np.array(self.curve_radius_f(df, 0, self.tick_nr, c1, c2, c3, c4, c5)) / 1000.0, "--", label="predicted_curve")
        plt.legend()
        plt.show()
    
    def find_slipping_coefficients_legacy(self, car):
        df = self.get_data_for_car(car)
        def f(start, end, c1, c2, c3, c4, c5):
            results = []
            for i in xrange(start, end+1):
                cf = df.ix[i]
                if i>0:
                    old = df.ix[i-1]
                else:
                    old = cf
               
                curve_radius = old.curve_radius
                if old.speed**2 < np.abs(curve_radius*c4):
                    curve_radius = 0

                if curve_radius == 0:
                    a=c1*old.slipangle*old.speed + c2*old.slipspeed
                elif curve_radius > 0:
                    a=((c1*old.slipangle+c5)*old.speed) + c2*old.slipspeed + c3/np.sqrt(curve_radius)*old.speed**2
                else:
                    a=((c1*old.slipangle-c5)*old.speed) + c2*old.slipspeed - c3/np.sqrt(np.abs(curve_radius))*old.speed**2
                
                results.append(a)
                
            return results
        
        def f2(indices, c1, c2, c3, c4, c5):
            start = indices[0]
            end = indices[-1]
            results = f(start, end, c1, c2, c3, c4, c5)
            return [results[x-start] for x in indices]
        
        def f2_non_continuous(indices, c1, c2, c3, c4, c5):
            return [f(x, x, c1, c2, c3, c4, c5)[0] for x in indices]
        
        def f2_fixed_limit(c4):
            def inner(indices, c1, c2, c3, c5):
                start = indices[0]
                end = indices[-1]
                results = f(start, end, c1, c2, c3, c4, c5)
                return [results[x-start] for x in indices]
            return inner
        
        straights = df[(df.piece == df.piece.shift()) & (df.curve_radius==0) & (np.abs(df.slipangle)>0.01) & (df.is_switch==False) & (df.shift().is_switch==False) & (df.acceleration<1)]
        if len(straights.index.values) >= 20:
            subset = straights
            x = np.array(subset.index.values)
            y = np.array(subset.slipacc.tolist())
            results = optimize.curve_fit(f2_non_continuous, x, y)
            print "Result for straights, num pieces %i" % len(straights.index.values)
            print results
            c1, c2, c3, c4, c5 = results[0]
            #plt.plot(straights.index.values, f2(straights.index.values, c1, c2, c3, c4, c5, c6), "--", label="predicted")
            #plt.plot(straights.index.values, straights.slipacc, label="actual")
            #plt.plot(straights.index.values, f2(straights.index.values, c1, c2, c3, c4, c5, c6)- straights.slipacc, "--", label="predicted-actual")
            #plt.plot(straights.index.values, f2(straights.index.values, 0, c2, c3, c4, c5, c6)- straights.slipacc, "--", label="predicted-actual, no angle")
            #plt.plot(straights.index.values, f2(straights.index.values,c1, 0, c3, c4, c5, c6)- straights.slipacc, "--", label="predicted-actual, no speed")
            
            #plt.plot(straights.index.values, straights.slipangle*c1, label="slipangle")
            #plt.plot(straights.index.values, straights.slipspeed*c2, label="slipspped")
            #plt.plot(straights.index.values, c2*(straights.slipangle-straights.slipspeed), label="slipspeed")
            #plt.plot(straights.index.values, straights.acceleration, label="aceleration")
            #plt.plot(straights.index.values, straights.speed / 50.0, label="speed")
            #plt.plot(straights.index.values, straights.throttle / 2, label="throttle")
            #plt.plot(straights.index.values, [0.0 for _ in xrange(len(straights.index.values))], "o")
            #plt.legend()
            #plt.show()      
        else:
            df["highest_limits"] = df.apply(lambda x: 0 if x.curve_radius==0 else x.speed**2/np.abs(x.curve_radius), axis = 1)
            df.sort("highest_limits", inplace=True, ascending=True)
            sorted_limits = df[((np.abs(df.slipangle)>10) & (df.piece == df.piece.shift()))].head(20)
            x = np.array(sorted_limits.index.values)
            y = np.array(sorted_limits.slipacc.tolist())
            df.sort_index(inplace=True)
            results = optimize.curve_fit(f2_non_continuous, x, y)
            print "Result for approximated straights"
            print results
            c1, c2, c3, c4, c5 = results[0]
            #plt.plot(df.index.values, f2(df.index.values, c1, c2, c3, c4, c5), "--", label="predicted")
            #plt.plot(df.index.values, df.slipacc, label="actual")
            #plt.plot(df.index.values, df.curve_radius / 1000, label="curve_radius")
            #plt.legend()
            #plt.show()  
            
        def get_under_limit(start, end, c1, c2):
            results = []
            for i in xrange(start, end+1):
                cf = df.ix[i]
                if i>0:
                    old = df.ix[i-1]
                else:
                    old = cf

                if old.curve_radius == 0 or old.is_switch or cf.is_switch:
                    results.append(0.0)
                else:
                    under = c1*old.slipangle*old.speed + c2*old.slipspeed
                    if np.abs(under -  cf.slipacc) < 0.001:
                        results.append(old.speed**2/np.abs(old.curve_radius))
                    else:
                        results.append(0.0)
                
            return results
        
        df["under_limit"] = get_under_limit(df.index.values[0], df.index.values[-1], c1, c2)
        
        c4 = df.under_limit.max()
        
        x = np.array(df[(df.is_switch==False) & (df.shift(2).is_switch==False) & (np.isclose(df.curve_radius, 110.0)) & (df.slipacc>0.0)].tail(20).index.values)
        y = np.array(df[(df.is_switch==False) & (df.shift(2).is_switch==False) & (np.isclose(df.curve_radius, 110.0)) & (df.slipacc>0.0)].tail(20).slipacc.tolist())
        results = optimize.curve_fit(f2_fixed_limit(c4), x, y)     
        
        c1, c2, c3, c5 = results[0]
        print "Final results"
        print results
        print c1, c2, c3, c4, c5
        
        plt.plot(df.index.values, df.slipacc, label="actual")
        plt.plot(df.index.values, f2(df.index.values, c1, c2, c3, c4, c5), "--", label="predicted")
        #plt.plot(df.index.values, (f2(df.index.values, c1, c2, c3, c4, c5)- df.slipacc), "--", label="predicted-actual")
        #plt.plot(df.index.values, f2(df.index.values, c1, c2, c3, 1000, c5), "--", label="predicted_under_limit")
        plt.plot(df.index.values, df.curve_radius / 1000, label="curve_radius")
        #plt.plot(df.index.values, df.slipangle*np.abs(c1), label="slipangle")
        #plt.plot(df.index.values, df.slipspeed*np.abs(c2), label="slipspped")
        #plt.plot(df.index.values, df.acceleration, label="acceleration")
        #plt.plot(df.index.values, df.throttle / 2, label="throttle")
        #plt.plot(df.index.values, df.speed / 50.0, label="speed")
        plt.plot(df.index.values, df.is_switch / 10.0, label="switch")
        #plt.plot(df.index.values, [0.0 for _ in xrange(len(df.index.values))], "o")
        #plt.plot(df.index.values, (df.curve_radius == 90) / 5.0 )
        plt.legend()
        plt.show()
        
        return (c1, c2, c3, c4, c5)
        
