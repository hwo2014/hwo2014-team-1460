#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import simulation.math2d as math2d
from simulation.switch_database import SwitchDatabase

class SimulatorBase(object):

    def __init__(self, track, cars, bots = [], switch_database = SwitchDatabase()):
        self.track = track
        self.cars = cars
        self.bots = bots
        self.bots += [None]*(len(self.cars) - len(self.bots))        
        self.switch_database = switch_database
        
    def initialize_starting_positions(self):
        for idx, car in enumerate(self.cars):
            car.lane = idx % len(self.track.track_settings.lanes)
            car.end_lane = car.lane
            car.position = self.track.pieces[0].get_lane(self.track.track_settings.lanes[car.lane], 0.0)
            car.piece = 0
            car.piece_distance = 0.0
            car.angle = 0.0
            car.switch = 0
            car.throttle = 0.0
            car.distance = 0.0
            car.speed = 0.0
            car.acceleration = 0.0
            car.slipspeed = 0.0
            car.slipacc = 0.0
            car.distance = 0.0
            car.crash_duration = 0
            car.turbo_duration = 0
            car.turbo_factor = 1.0
            car.turbo = False
            car.turbo_available = None
            car.turbo_activation = None
            self.update_car_position_from_piece_distance(car)
            self.update_car_angle_with_slip_angle(car, 0.0)

    def update_car_position_from_piece_distance(self, car):
        lane_pos = self.track.track_settings.lanes[car.lane]
        piece = self.track.pieces[car.piece]
        relative_distance = piece.distance_to_relative_for_lane(car.piece_distance, lane_pos)
        car.position = piece.get_lane(lane_pos, relative_distance)
        
    def update_car_angle_with_slip_angle(self, car, angle):
        lane_pos = self.track.track_settings.lanes[car.lane]
        piece = self.track.pieces[car.piece]
        relative_distance = piece.distance_to_relative_for_lane(car.piece_distance, lane_pos)
        car.direction = math2d.rotate_vector_deg(piece.get_tangent(relative_distance), angle)
        
    def update_bots(self):
        for index, car in enumerate(self.cars):
            bot = self.bots[index]
            if bot is not None:
                bot.command = None
                bot.update(car, self)
                if car.crash_duration==0:
                    if bot.command is not None:
                        command, value = bot.command
                        if command == "throttle":
                            car.throttle = value
                        elif command == "switch":
                            car.switch = value
                        elif command =="turbo":
                            car.turbo = True
                