#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import numpy as np
import math
import simulation.math2d as math2d

class Car(object):

    def __init__(self, color, length, width, guide_position):
        self.color = color
        self.position = np.array((0, 0))
        self.dimensions = np.array((width, length))
        self.direction = np.array((0, 1))
        self.guide_position = guide_position
        
    def get_bounding_box(self):
        half_width = self.dimensions[0] / 2.0
        length = self.dimensions[1]
        angle = math2d.direction_to_angle_rad(self.direction, (0.0, 1.0))
        box = np.array([[-half_width, 0], [half_width, 0], [half_width, -length], [-half_width, -length]])
        box += (0.0, self.guide_position)
        rot_matrix = math2d.rotation_matrix_rad(angle)
        box = np.dot(rot_matrix, box.T).T
        box += self.position
        return np.array(box)