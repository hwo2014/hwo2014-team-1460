#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import math
import logging
import sys
import pyglet
import argparse
import os
from simulation.track import Track
from simulation.track_piece import TrackPiece
from simulation.car import Car
from simulation.simulator import Simulator
from simulation.replay_simulator import ReplaySimulator
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from visualization.track_visualizer import TrackVisualizer
from visualization.car_visualizer import CarVisualizer
from race_control.replay_race import ReplayRace
from race_control.client_race import ClientRace
from race_control.server_race import ServerRace
from pyglet.gl import *
import pyglet.window.key as key
import numpy as np
import IPython
from IPython.terminal.embed import InteractiveShellEmbed
from IPython.config.loader import Config
import matplotlib.pyplot as plt

background_color = [1.0, 1.0, 1.0, 0.0]
car_color = [1.0, 0, 0]

logger = logging.getLogger("jyckegarden.visualization")
logger.setLevel(logging.INFO)
h = logging.StreamHandler(sys.stdout)
logger.addHandler(h)

def calculate_extents(width, height, track, border):    
    track_extents = track_visualizer.get_extents()
     
    track_width = track_extents[1][0]-track_extents[0][0]
    track_height = track_extents[1][1]-track_extents[0][1]
    track_aspect = track_width / track_height
    screen_aspect = float(width) / float(height)
    logger.debug("Resize %i, %i aspect: %f" % (width, height, screen_aspect))

    border = float(border) * (float(track_width) / float(width))
    
    if track_aspect > screen_aspect:
        min_x = track_extents[0][0] - border
        max_x = track_extents[1][0] + border
        x_width = max_x - min_x
        y_height = x_width / screen_aspect
        min_y = track_extents[0][1] - border
        max_y = min_y + y_height  
        logger.debug("this")
    else:
        min_y = track_extents[0][1] - border
        max_y = track_extents[1][1] + border
        y_height = max_y - min_y
        x_width = y_height * screen_aspect
        min_x = track_extents[0][0] - border
        max_x = min_x + x_width
    
    logger.debug("Display extents %f, %f, %f %f, aspect %f" % (min_x, max_x, min_y, max_y, (max_x - min_x) / (max_y-min_y)))
    return (min_x, max_x, min_y, max_y)
    

class Updater(object):
    
    def __init__(self, race):
        self.time = 0.0
        self.current_step = 0
        self.ipython_started = False
        
        self.time_multipliers = [
            10.0,
            5.0,
            2.0,
            1,
            0.5,
            0.2,
            0.1,
            0.05
        ]
        self.paused = False
        self.time_multiplier = 0
        self.set_time_multiplier(self.time_multipliers.index(1))
        self.step_time = 1.0 / 60.0
        self.simulator = race.simulator
        self.race = race
        
        self.ipshell = InteractiveShellEmbed()
        self.ipshell.enable_matplotlib("auto")
    def set_time_multiplier(self, multiplier):
        if multiplier>=0 and multiplier < len(self.time_multipliers):
            logger.info("Time multiplier set to %f" % (self.time_multipliers[multiplier]))
            self.time_multiplier = multiplier
    
    def increase_time_multiplier(self):
        self.set_time_multiplier(self.time_multiplier - 1)

    def decrease_time_multiplier(self):
        self.set_time_multiplier(self.time_multiplier + 1)
    
    def update(self, dt):
        if type(self.race) == ServerRace:
            self.race.tick()  
        else:
            #logger.debug(dt)
            if self.ipython_started:
                #Skip one update after resuming from IPython
                self.ipython_started = False
            elif not self.paused:
                self.time+=dt * self.time_multipliers[self.time_multiplier]
                while(self.current_step*self.step_time < self.time):
                    self.current_step += 1
                    self.race.tick()
                
    def one_forward(self):
        self.current_step += 1
        self.race.tick()
        self.time+=self.step_time
        
    def start_ipython(self):
        self.ipshell()
        self.ipython_started = True



if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description="SlotRace visualizer")
    subparsers = parser.add_subparsers(dest="subparser_name", help="sub-command help")
    
    replayparser = subparsers.add_parser("replay", help="show a replay")
    replayparser.add_argument("filename")
    replayparser.add_argument("--bot", default=None, help="Add a bot to the specified color")

    clientparser = subparsers.add_parser("client", help="Run a race on the client")
    clientparser.add_argument("trackname")
    
    serverparser = subparsers.add_parser("server", help="Run a race on the server")
    serverparser.add_argument("host")
    serverparser.add_argument("port")
    serverparser.add_argument("botname")
    serverparser.add_argument("botkey")
    serverparser.add_argument("password")
    serverparser.add_argument("--log", default=None, help="Specify location of logfile, if needed")
    serverparser.add_argument("--trackname", default=None, help="The name of the track to race on")
    serverparser.add_argument("--num_cars", default=1, help="Create or join a multiplayer race")
    serverparser.add_argument("--novisual", action="store_true")
    
    args = parser.parse_args()
    
    if args.subparser_name == "replay":
        race = ReplayRace(args.filename, args.bot)
    elif args.subparser_name == "client":
        track_file_name = os.path.join("tracks", args.trackname) + ".txt"
        race = ClientRace(track_file_name)
    elif args.subparser_name == "server":
        race = ServerRace(args.host, args.port, args.botname, args.botkey, log_file=args.log, trackname=args.trackname, car_count=args.num_cars, password = args.password)
        if args.novisual:
            while not race.tournament_ended:
                race.tick()
            sys.exit()
        
    window = pyglet.window.Window(resizable = True)  
    
    track_visualizer = TrackVisualizer(
                                       track = race.track,
                                       tarmac_color = [0.0, 0.0, 0],
                                       lane_color = [0.5, 0.5, 0.5],
                                       lane_width = 3,
                                       section_joint_color = [0.5, 0.5, 0.5],
                                       section_joint_width = 1,
                                      )
    
    car_visualizer = CarVisualizer(race)

    @window.event
    def on_draw():
        glDisable(GL_DEPTH_TEST)
        glDisable(GL_LIGHTING);
        glClearColor(*background_color)
        glClear(GL_COLOR_BUFFER_BIT)
        track_visualizer.draw()
        car_visualizer.draw()
        
        
    @window.event
    def on_resize(width, height):
        glViewport(0, 0, width, height)
        glMatrixMode(gl.GL_PROJECTION)
        glLoadIdentity()
        min_x, max_x, min_y, max_y = calculate_extents(width, height, race.track, 50) 
        glOrtho(min_x, max_x, min_y, max_y, -1, 1)
        glMatrixMode(GL_MODELVIEW)
        glEnable(GL_MULTISAMPLE)
        glEnable(GL_LINE_SMOOTH);
        return pyglet.event.EVENT_HANDLED

    updater = Updater(race)
    def update(dt):
        updater.update(dt)
     
    @window.event    
    def on_key_press(symbol, modifiers):
        if symbol==key.LEFT:
            updater.decrease_time_multiplier()
        elif symbol==key.RIGHT:
            updater.increase_time_multiplier()
        elif symbol==key.UP:
            updater.one_forward()
        elif symbol==key.P:
            updater.paused = not updater.paused
        elif symbol==key.SPACE:
            updater.start_ipython()

    pyglet.clock.schedule_interval(update, 1.0 / 120.0)
    pyglet.app.run()
