#===============================================================================
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#===============================================================================

import argparse
from simulation.track_builder import TrackBuilder
from simulation.car_builder import CarBuilder
from simulation.analyzing_simulator import AnalyzingSimulator
from simulation.replay_simulator import ReplaySimulator
from simulation.switch_database import SwitchDatabase


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Switch database generator")
    parser.add_argument("input")
    parser.add_argument("output")
    
    args = parser.parse_args()
    
    switch_database = SwitchDatabase.load(args.output)
    print "Num straights", len(switch_database.straight_database)
    for k, v in switch_database.straight_database:
        print k
    print "Num known curves", len(switch_database.curve_formula)
    for k, v in switch_database.curve_formula.items():
        print k
    print "Num unknown curves", len(switch_database.curve_approximation)
    for k, v in switch_database.curve_approximation.items():
        print k    
    with open(args.input, "r") as f:
        jsonstring = f.read()
        track = TrackBuilder().build(jsonstring)
        cars = CarBuilder().build(jsonstring)        
        analyzer = AnalyzingSimulator(ReplaySimulator(track = track, cars = cars, owner=0, jsontext = jsonstring), switch_database=switch_database)
        analyzer.tick_until_done()
        database = analyzer.switch_database
        database.save(args.output)
        print "Num straights", len(switch_database.straight_database)
        for k, v in switch_database.straight_database:
            print k
        print "Num known curves", len(switch_database.curve_formula)
        for k, v in switch_database.curve_formula.items():
            print k
        print "Num unknown curves", len(switch_database.curve_approximation)
        for k, v in switch_database.curve_approximation.items():
            print k
